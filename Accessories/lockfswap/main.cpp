#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>       // std::cout, std::endl
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include <fcntl.h>
#include <unistd.h>

#include <sys/file.h>



int lockfile(char*sourcefile)
{
  int fildes;
  int status;
  
  fildes = open(sourcefile, O_RDWR|O_CREAT,S_IRUSR|S_IWUSR|S_IRGRP);
  
  if (fildes == -1)
  {
    printf("can't open file. exist.\n");
    exit(-1);
    return -1;
  }
  
  lseek(fildes,(off_t)0,SEEK_SET);  
  status = lockf(fildes, F_LOCK, (off_t)0);

  if (status == -1)
  {
    printf("lockf failed.\n");
    exit(-1);
    return -1;
  }    

  return fildes;
}

int unlockfile(int fildes)
{
  int status;
  lseek(fildes,(off_t)0,SEEK_SET);
  status = lockf(fildes, F_ULOCK, (off_t)0);
  
  if(status == -1)
  {
    printf("lockf failed.\n");
    exit(-1);
    return 0;
  }

  status=close(fildes);
  return 0;
}

char strinput[2000];
void chomp(char *s)
{
  while(*s && *s != '\n' && *s != '\r') s++;
  *s = 0;
}
int getinput()
{
  fgets(strinput,2000, stdin);
  chomp(strinput);
  return 0;
}

int readfile(int fildes,char**ptrbuf,int*ptrsize)
{
  int filesize;
  int readret;
  filesize=lseek(fildes,(off_t)0,SEEK_END);
  lseek(fildes,(off_t)0,SEEK_SET);
  char *buf = (char*)malloc(filesize + 1);
  if(buf == NULL)
  {
    printf("malloc failed.\n");
    exit(-1);
    return 0;
  }
  
  readret=read(fildes,buf,filesize);
  (*ptrbuf)=buf;
  (*ptrsize)=filesize;
  
  printf("ret:%d\nlen:%d\n",readret,filesize);
  return 0;
}

int readfile(char*filename,char**ptrbuf,int*ptrsize)
{
  int fildes;
  int retcode;
  
  fildes = open(filename, O_RDWR|O_CREAT,S_IRUSR|S_IWUSR|S_IRGRP);
  
  if (fildes == -1)
  {
    printf("can't open file. exist.\n");
    exit(-1);
    return -1;
  }
  
  retcode=readfile(fildes,ptrbuf,ptrsize);
  
  close(fildes);
  
  return retcode;
}

int copyfile(char*filename,char*buf,int filesize)
{
  int fildes;
  
  fildes = open(filename, O_RDWR|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP);
  
  if (fildes == -1)
  {
    printf("can't open file. exist.\n");
    exit(-1);
    return -1;
  }
  
  write(fildes,buf,filesize);
  close(fildes);
  return 0;
}

int writelog(char*filename,char*buf)
{
  int fildes;
  fildes = open(filename, O_RDWR|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP);
  
  if(fildes == -1)
  {
    printf("can't open log file. exist.\n");
    exit(-1);
    return -1;
  }
  
  write(fildes,buf,strlen(buf));
  close(fildes);
}

int main(int argc, char **argv)
{
  if(argc<4)
  {
     printf("wrong argument. Usage: lockfswap [source] [copy] [log]\n");
     return -1;
  }
  char*strsource=argv[1];
  char*strcopy=argv[2];
  char*strlog=argv[3];
  
  int srcfd=lockfile(strsource);
  char*filecontent;
  int filesize;
  readfile(srcfd,&filecontent,&filesize);
  copyfile(strcopy,filecontent,filesize);
  free(filecontent);
  
  printf("lock and copy %s to %s. Input updated copy:\n",strsource,strcopy);
  writelog(strlog,(char*)"Copy done.");
  
  getinput();
  
  readfile(strinput,&filecontent,&filesize);
  //printf("length : %d\ncontent: %s\n",filesize,filecontent);
  
  ftruncate(srcfd,(off_t)0);
  lseek(srcfd,(off_t)0,SEEK_SET);
  write(srcfd,filecontent,filesize);
  
  free(filecontent);  
  unlockfile(srcfd);
  
  printf("copy %s to %s and unlock\n",strinput,strsource);
  writelog(strlog,(char*)"Update done.");
    
  return 0;
}


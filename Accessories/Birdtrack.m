(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



MyRowReduce[matrix_]:=RowReduce[matrix];


(* return indices such that list1[[indices]]==list2 *)
FindPermutationIndices[list1_List,list2_List]:=Module[
{perm,indices},
perm=FindPermutation[list1,list2];
If[Head@perm===FindPermutation,Return[False]];
indices=Permute[Range[1,Length@list1],perm];
indices
];

PermutationSignature[perm_?PermutationCyclesQ]:=Apply[Times,(-1)^(Length/@First[perm]-1)];
FindPermutationSign[list1_List,list2_List]:=Signature[list1]Signature[list2];


GenerateUniqueIndices$Counter=1;
GenerateUniqueIndex[dim_:1]:=Index[GenerateUniqueIndices$Counter++,dim];
GenerateUniqueIndexList[dim_:1,NN_:1]:=Table[GenerateUniqueIndex[dim],NN];

GenerateGroupIndices[structure_List]:=GenerateGroupIndices/@structure;
GenerateGroupIndices[type_/;Head@type=!=List]:=GenerateUniqueIndex[type];

AutoEqu$GenerateProjectorIndices[projobj_,correlator_List]:=Module[
{typelist},
typelist=ObjGet[projobj,IndexType@#,Default->"Missing"]&/@correlator;
If[Count[typelist,"Missing"]>0,Print["AutoEqu$GenerateProjectorIndices error : no correct IndexType for correlator"];Abort[];];

GenerateGroupIndices@typelist
];


Clear@ObjSet;
SetAttributes[ObjSet,HoldFirst];
ObjSet[object_,itemname_,value_,newitemB_:False]:=Module[
{rule},
rule=itemname->value;

If[FreeQ[object,(itemname->_),{1}],
If[newitemB===False,Print["ObjSet warning:",itemname," was not in object set. ObjSet will add this member to the object"];];

AppendTo[object,rule];
Return[object];
];

object=Replace[object,(itemname->_)->(itemname->value),{1}];

Return[value];
];

ObjSet/: Set[ObjSet[obj_,item_],val_]:=ObjSet[obj,item,val];


Clear@PartSpecListQ;
PartSpecListQ[_List]:=True;
PartSpecListQ[_Span]:=True;
PartSpecListQ[All]:=True;
PartSpecListQ[_]:=False;

Clear@ObjGet;

ObjGet[object_List,itemname_?PartSpecListQ,rest__]:=ObjGet[#,rest]&/@object[[itemname]];

ObjGet[object_List,itemname_,rest__]:=ObjGet[ObjGet[object,itemname],rest];

ObjGet[object_List,itemname_Integer]:=object[[itemname]];
ObjGet[object_List,itemname_?PartSpecListQ]:=object[[itemname]];

ObjGet[object_List,itemname_]:=Module[
{item},
item=Cases[object,HoldPattern[itemname->_]];

Which[
Length@item==0,Print["ObjGet error: find 0 instance : itemname=",itemname];Print["object=",object];Abort[];,

Length@item>1,Print["ObjGet error: find multi instance : itemname=",itemname];
Print["object=",object];
Abort[];,

Length@item==1,Return[item[[1,2]]];
];
Print["ObjGet error 0"];Abort[];
];

ObjGet[object_List,itemname_,Default->default_]:=Module[
{item},
item=Cases[object,HoldPattern[itemname->_]];

Which[
Length@item==0,Return[default],

Length@item>1,Print["ObjGet error: find multi instance : itemname=",itemname];
Print["object=",object];
Abort[];,

Length@item==1,Return[item[[1,2]]];
];
Print["ObjGet error 0"];Abort[];
];


ObjExec[obj_][method_String,parameters___]:=ObjGet[obj,method][obj,parameters];


ObjDel[obj_,itemlist_List]:=DeleteCases[obj,Alternatives@@(HoldPattern[#->_]&/@itemlist)];
ObjDel[obj_,item_]:=DeleteCases[obj,HoldPattern[item->_]];


Resolve\[Epsilon][indicesA_,indicesB_]:=Module[
{allperm,rslt},
allperm=Permutations[indicesB];
rslt=Module[{sign,permB=#},
sign=FindPermutationSign[indicesB,permB];
sign Times@@Table[\[Delta][indicesA[[\[Epsilon]permrule$i]],permB[[\[Epsilon]permrule$i]]],{\[Epsilon]permrule$i,1,Length@indicesB}]
]&/@allperm;
Plus@@rslt];

Clear@Contract\[Delta];
Contract\[Delta][expr_]:=Module[{},
expr//.{
\[Delta][m_,n_]/;Not@OrderedQ[{m,n}]:>\[Delta][n,m],
\[Delta][A___,n_Index,B___]\[Delta][C___,n_Index,D___]:>\[Delta][A,B,C,D],
\[Delta][Index[a_,dim_],Index[a_,dim_]]:>dim,
\[Delta][Index[a_,dim_],Index[b_,dim_]]^2/;a!=b:>dim,
\[Epsilon][A___]/;Not@OrderedQ[{A}]:>Module[{newA=Sort@{A}},FindPermutationSign[newA,{A}]\[Epsilon][Sequence@@newA]],
\[Delta][A___,n_Index,B___]\[Epsilon][C___,n_Index,D___]:>\[Epsilon][C,A,B,D],
\[Epsilon][A___]\[Epsilon][B___]/;Length[{A}]===Length[{B}]:>Resolve\[Epsilon][{A},{B}],
\[Epsilon][A___]^2:>Resolve\[Epsilon][{A},{A}]
}
];

Simplify\[Delta]2[expr_]:=expr//Contract\[Delta]//Collect[#,_\[Delta],Simplify]&//Expand[#,_\[Delta]]&;
Simplify\[Delta][expr_]:=FixedPoint[Simplify\[Delta]2,expr];



(*V2*)
AutoEqu$GetAllProjectors[projobj_]:=Cases[projobj,HoldPattern[Projector[_]->_]]/.Projector->Identity;
AutoEqu$GetProjectors[projobj_,correlator_]:=Module[
{projlist,rslt,allcor,sort,sort$indexed,base,projs,perm$base2std,perm$cor2std,perm$cor2base,invperm,perm2std,permutefunc},

projlist=AutoEqu$GetAllProjectors[projobj];

If[(rslt=correlator/.projlist)=!=correlator,
Return[rslt];
];

allcor=projlist[[All,1]];

sort=Sort[Sort/@Partition[#,{2}]]&;
sort$indexed=Function[{str},
SortBy[SortBy[#,First]&/@Partition[MapIndexed[{#1,#2[[1]]}&,str],{2}],#[[All,1]]&]//Flatten[#,1]&
];

perm2std[c_]:=sort$indexed[c][[All,2]];
invperm[p_]:=Reverse/@Thread[Rule[{1,2,3,4},p]]//SortBy[#,First][[All,2]]&;

base=Select[projlist[[All,1]],sort@#==sort@correlator&];

If[Length@base!=1,
If[Length@base==0,Print["Can't find projector for ",correlator,". Abort."],
Print["Find multi projector instance for ",correlator," :",base,". Abort."]
];
Abort[];
];
base=base[[1]];

projs=base/.projlist;

perm$base2std=perm2std@base;
perm$cor2std=perm2std@correlator;
perm$cor2base=perm$cor2std[[invperm@perm$base2std]];

permutefunc[p_][func_]:=(With[{body=ProjFuncName[Sequence@@(Slot/@p)]},body&])/.ProjFuncName->func;

MapAt[permutefunc[perm$cor2base],projs,{All,2}]
];

(*V2*)
AutoEqu$ProjectorSign[projector_,ijkllist_]:=Module[
{ilist,jlist,klist,llist,getindextype},
getindextype[indices_]:=indices/.Index[_,dim_]:>dim;

{ilist,jlist,klist,llist}=ijkllist;

If[getindextype@ilist=!=getindextype@jlist,Return[0]];
If[Simplify\[Delta][projector[ilist,jlist,klist,llist]-projector[jlist,ilist,klist,llist]]==0,Return[1]];
If[Simplify\[Delta][projector[ilist,jlist,klist,llist]+projector[jlist,ilist,klist,llist]]==0,Return[-1]];
Return[0];
];

(*V2*)
AutoEqu$RepSigns[projobj_]:=Module[
{allprojname,signlist,errorcases},

signlist=Module[{cor=#[[1]],projlist=#[[2]],ijkllist},
ijkllist=AutoEqu$GenerateProjectorIndices[projobj,cor];

MapAt[AutoEqu$ProjectorSign[#,ijkllist]&,projlist,{All,2}]
]&/@AutoEqu$GetAllProjectors[projobj];

signlist=Flatten@signlist//DeleteDuplicates;

errorcases=Select[GatherBy[signlist,First],Length@#>1&];
If[Length@errorcases>0,Print["AutoEqu$AllProjectorSign error : contradicting cases : ",errorcases];Abort[];];

signlist
];

(*V2*)
(* give M defined by Subscript[Q, R]=\!\(
\*SubscriptBox[\(\[Sum]\), \(S\)]\(
\*SubscriptBox[\(M\), \(R\ S\)]
\*SubscriptBox[\(P\), \(S\)]\)\) *)
AutoEqu$MMatrixFromProjs[Dprojs_List,Eprojs_List,ijkllist_]:=Module[
{DprojN=Length@Dprojs,ilist,jlist,klist,llist},
{ilist,jlist,klist,llist}=ijkllist;

Module[{Eproj=#,coeff=Table[Subscript[x, i],{i,1,DprojN}],equ,equcomp,sol},
equ=Eproj[klist,jlist,ilist,llist]-coeff.(#[ilist,jlist,klist,llist]&/@Dprojs)//Expand//Contract\[Delta]//Simplify\[Delta]//Collect[#,_\[Delta]|_\[Epsilon],\[Delta]Coeff]&;

equcomp=Cases[equ,\[Delta]Coeff[a_]:>a==0,{0,Infinity}];

sol=equcomp//Solve[#,coeff]&;

If[Length@sol==0,
Print["Crossing for projectors has no solution. Check completeness of the projectors. Abort."];
Print["projectors : ",{Eprojs,Dprojs}];
Abort[];];

sol[[1,All,2]]
]&/@Eprojs
];

(*V2*)
Clear@AutoEqu$MMatrix;
AutoEqu$MMatrix[projobj_,correlator_]:=Module[
{dprojs,eprojs,Mmatrix,ijkllist},
dprojs=AutoEqu$GetProjectors[projobj,correlator];
eprojs=AutoEqu$GetProjectors[projobj,correlator[[{3,2,1,4}]]];
ijkllist=AutoEqu$GenerateProjectorIndices[projobj,correlator];

Mmatrix=AutoEqu$MMatrixFromProjs[dprojs[[All,2]],eprojs[[All,2]],ijkllist];

{Mmatrix,dprojs[[All,1]],eprojs[[All,1]]}
];

AutoEqu$Exchange31[list4_List]:=list4[[{3,2,1,4}]];

AutoEqu$SingleTerm[coeff_,OpsDef_,vacrep_,opslist_,replist_,rep_,FHsign_]:=Module[
{externalOPs,rslt,
rep$name,rep$leftindex,rep$rightindex,rep$purename,rep$CC},
{rep$name,rep$leftindex,rep$rightindex,rep$purename,rep$CC}=ParseRep[rep];

rslt=0;
If[rep$name===vacrep && SameQ@@opslist[[1;;2]] && SameQ@@opslist[[3;;4]],rslt=coeff single[FHsign[Sequence@@opslist,0]]];

If[Not@MemberQ[OpsDef[[All,2]],rep$name],Return[rslt]];

externalOPs=Select[OpsDef,#[[2]]===rep$name&][[All,1]];

rslt=rslt+Sum[
coeff single[FHsign[Sequence@@opslist,internalOP] 
 \[Beta][op[opslist[[1]],replist[[1]],1,1],op[opslist[[2]],replist[[2]],1,1],op[internalOP,rep$name,1,1]][rep$leftindex]
\[Beta][op[opslist[[3]],replist[[3]],1,1],op[opslist[[4]],replist[[4]],1,1],op[internalOP,rep$name,1,1]][rep$rightindex]
]
,{internalOP,externalOPs}];

rslt
];

AutoEqu$GenerateEqu$SingleCorrelator[projobj_,OpsDef_,vacrep_,correlator_]:=Module[
{ops2rep,opslist,replist,correps,Dreps,Ereps,Mmatrix,Mdim,Meven,Modd,Fequ,Hequ},
opslist=correlator;

ops2rep=OpsDef/.op->Rule;
replist=opslist/.ops2rep;
correps=replist;

{Mmatrix,Dreps,Ereps}=AutoEqu$MMatrixV2[projobj,correps];

(* abac case : reduce to (\[PlusMinus]M^T-1)Subscript[F, \[PlusMinus]]=0
If correlator is in not abac case, the only other possible to reduce to (\[PlusMinus]M^T-1)Subscript[F, \[PlusMinus]]=0 form is aaba case. But this case is degenerate, we assume we never use this form (use abaa instead).
 *)
If[SameQ@@correlator[[{1,3}]],

Mdim=Length@Mmatrix;

Meventest=Transpose[Mmatrix]-IdentityMatrix[Mdim];
Moddtest=-Transpose[Mmatrix]-IdentityMatrix[Mdim];

Meven=Transpose[Mmatrix]-IdentityMatrix[Mdim]//MyRowReduce//DeleteCases[#,Table[0,Mdim]]&;
Modd=-Transpose[Mmatrix]-IdentityMatrix[Mdim]//MyRowReduce//DeleteCases[#,Table[0,Mdim]]&;

If[Length@Meven+Length@Modd!=Mdim,
Print["Error: Length@Meven+Length@Modd\[NotEqual]Mdim for ",correlator];
Print["Length@Modd=",Length@Modd," Length@Meven=",Length@Meven," Mdim=",Mdim];
];

(* \!\(
\*SubscriptBox[\(\[Sum]\), \(R\)]\(
\*SubscriptBox[\(\[Sum]\), \(\[ScriptCapitalO] \[Element] R\)]\((
\*SubscriptBox[\(\[Lambda]\), \(i\ j\ \[ScriptCapitalO]\)]
\*SubscriptBox[\(\[Lambda]\), \(i\ l\ \[ScriptCapitalO]\)])\)\ 
\*SubscriptBox[\((\(\[PlusMinus]
\*SuperscriptBox[\(M\), \(T\)]\) - \[DoubleStruckOne])\), \(S, R\)]
\*SubsuperscriptBox[\(F\), \(\[PlusMinus]\(\(,\)\(\[ScriptCapitalO]\)\)\), \(i\ j\ i\ l\)]\)\)=0 where R labels reps in exchanged channel *)
Fequ=Table[
Sum[
Module[{sum$evenspin,sum$oddspin,sum$external,spinselection,
Erep$name,Erep$leftindex,Erep$rightindex,rep$purename,rep$CC},
{Erep$name,Erep$leftindex,Erep$rightindex,rep$purename,rep$CC}=ParseRep[Ereps[[R]]];

{sum$evenspin,sum$oddspin}=Table[
Modd[[S,R]] sum[F[Sequence@@opslist]
 \[Beta][op[opslist[[1]],replist[[1]],1,1],op[opslist[[2]],replist[[2]],1,1],op[op,Erep$name,1,sign]][Erep$leftindex]
\[Beta][op[opslist[[3]],replist[[3]],1,1],op[opslist[[4]],replist[[4]],1,1],op[op,Erep$name,1,sign]][Erep$rightindex]
,op[op,Erep$name,1,sign]]
,{sign,{1,-1}}];

sum$external=AutoEqu$SingleTerm[Modd[[S,R]],OpsDef,vacrep,AutoEqu$Exchange31@opslist,AutoEqu$Exchange31@replist,Ereps[[R]],Fp];

spinselection=AutoEqu$SpinSelection[projobj,AutoEqu$Exchange31@opslist,AutoEqu$Exchange31@replist,Ereps[[R]]];

Switch[spinselection,
1,sum$evenspin+sum$external,
-1,sum$oddspin,
0,sum$evenspin+sum$oddspin+sum$external
]
]
,{R,Length@Ereps}]
,{S,Length@Modd}]/.sum[0,__]:>0;

Hequ=Table[
Sum[
Module[{sum$evenspin,sum$oddspin,sum$external,spinselection,
Erep$name,Erep$leftindex,Erep$rightindex,rep$purename,rep$CC},
{Erep$name,Erep$leftindex,Erep$rightindex,rep$purename,rep$CC}=ParseRep[Ereps[[R]]];

{sum$evenspin,sum$oddspin}=Table[
Meven[[S,R]] sum[H[Sequence@@opslist]
 \[Beta][op[opslist[[1]],replist[[1]],1,1],op[opslist[[2]],replist[[2]],1,1],op[op,Erep$name,1,sign]][Erep$leftindex]
\[Beta][op[opslist[[3]],replist[[3]],1,1],op[opslist[[4]],replist[[4]],1,1],op[op,Erep$name,1,sign]][Erep$rightindex],
op[op,Erep$name,1,sign]]
,{sign,{1,-1}}];

sum$external=AutoEqu$SingleTerm[Meven[[S,R]],OpsDef,vacrep,AutoEqu$Exchange31@opslist,AutoEqu$Exchange31@replist,Ereps[[R]],Hp];

spinselection=AutoEqu$SpinSelection[projobj,AutoEqu$Exchange31@opslist,AutoEqu$Exchange31@replist,Ereps[[R]]];

Switch[spinselection,
1,sum$evenspin+sum$external,
-1,sum$oddspin,
0,sum$evenspin+sum$oddspin+sum$external
]
]
,{R,Length@Ereps}]
,{S,Length@Meven}]/.sum[0,__]:>0;

Return[Fequ~Join~Hequ]
];

(* Not abac case : reduce to \[PlusMinus]M^T\[CenterDot]Subscript[F, \[PlusMinus]]=Subscript[F, \[PlusMinus]]
 *)
Mdim=Length@Mmatrix;
Meven=Transpose[Mmatrix]//MyRowReduce//DeleteCases[#,Table[0,Mdim]]&;
Modd=-Transpose[Mmatrix]//MyRowReduce//DeleteCases[#,Table[0,Mdim]]&;

If[Length@Modd!=Mdim || Length@Meven!=Mdim,
Print["Error: Length@Modd(or Meven)\[NotEqual]Mdim for ",correlator];
Print["Length@Modd=",Length@Modd," Length@Meven=",Length@Meven," Mdim=",Mdim];
];


(* \!\(
\*SubscriptBox[\(\[Sum]\), \(R\)]\(
\*SubscriptBox[\(\[Sum]\), \(\[ScriptCapitalO] \[Element] R\)]\(\[PlusMinus]\((
\*SubscriptBox[\(\[Lambda]\), \(k\ j\ \[ScriptCapitalO]\)]
\*SubscriptBox[\(\[Lambda]\), \(i\ l\ \[ScriptCapitalO]\)])\)\)\ 
\*SubscriptBox[\((
\*SuperscriptBox[\(M\), \(T\)])\), \(S, R\)]
\*SubsuperscriptBox[\(F\), \(\[PlusMinus]\(\(,\)\(\[ScriptCapitalO]\)\)\), \(k\ j\ i\ l\)]\)\)=\!\(
\*SubscriptBox[\(\[Sum]\), \(\[ScriptCapitalO] \[Element] S\)]\(\((
\*SubscriptBox[\(\[Lambda]\), \(i\ j\ \[ScriptCapitalO]\)]
\*SubscriptBox[\(\[Lambda]\), \(k\ l\ \[ScriptCapitalO]\)])\)
\*SubsuperscriptBox[\(F\), \(\[PlusMinus]\(\(,\)\(\[ScriptCapitalO]\)\)\), \(i\ j\ k\ l\)]\)\) where R labels reps in exchanged channel *)
{Fequ,Hequ}=Table[
Table[
Module[{LHS,RHS,FH,FHp},
FH=FHsign/.{-1->F,1->H};
FHp=FHsign/.{-1->Fp,1->Hp};

LHS=Sum[
Module[{sum$evenspin,sum$oddspin,sum$external,spinselection,
Erep$name,Erep$leftindex,Erep$rightindex,rep$purename,rep$CC},
{Erep$name,Erep$leftindex,Erep$rightindex,rep$purename,rep$CC}=ParseRep[Ereps[[R]]];

{sum$evenspin,sum$oddspin}=Table[
FHsign Transpose[Mmatrix][[S,R]]sum[FH[Sequence@@AutoEqu$Exchange31@opslist] 
\[Beta][op[opslist[[3]],replist[[3]],1,1],op[opslist[[2]],replist[[2]],1,1],op[op,Erep$name,1,sign]][Erep$leftindex]
\[Beta][op[opslist[[1]],replist[[1]],1,1],op[opslist[[4]],replist[[4]],1,1],op[op,Erep$name,1,sign]][Erep$rightindex]
,op[op,Erep$name,1,sign]]
,{sign,{1,-1}}];

sum$external=AutoEqu$SingleTerm[FHsign Transpose[Mmatrix][[S,R]],OpsDef,vacrep,AutoEqu$Exchange31@opslist,AutoEqu$Exchange31@replist,Ereps[[R]],FHp];

spinselection=AutoEqu$SpinSelection[projobj,AutoEqu$Exchange31@opslist,AutoEqu$Exchange31@replist,Ereps[[R]]];

Switch[spinselection,
1,sum$evenspin+sum$external,
-1,sum$oddspin,
0,sum$evenspin+sum$oddspin+sum$external
]
]
,{R,Length@Ereps}];

RHS=Module[{RHS$evenspin,RHS$oddspin,RHS$single,spinselection,
Drep$name,Drep$leftindex,Drep$rightindex,rep$purename,rep$CC},
{Drep$name,Drep$leftindex,Drep$rightindex,rep$purename,rep$CC}=ParseRep[Dreps[[S]]];

{RHS$evenspin,RHS$oddspin}=Table[
sum[FH[Sequence@@opslist] 
\[Beta][op[opslist[[1]],replist[[1]],1,1],op[opslist[[2]],replist[[2]],1,1],op[op,Drep$name,1,sign]][Drep$leftindex]
\[Beta][op[opslist[[3]],replist[[3]],1,1],op[opslist[[4]],replist[[4]],1,1],op[op,Drep$name,1,sign]][Drep$rightindex]
,op[op,Drep$name,1,sign]]
,{sign,{1,-1}}];

RHS$single=AutoEqu$SingleTerm[1,OpsDef,vacrep,opslist,replist,Dreps[[S]],FHp];

spinselection=AutoEqu$SpinSelection[projobj,opslist,replist,Dreps[[S]]];

Switch[spinselection,
1,RHS$evenspin+RHS$single,
-1,RHS$oddspin,
0,RHS$evenspin+RHS$oddspin+RHS$single
]
];

LHS-RHS
]
,{S,Length@Dreps}]
,{FHsign,{-1,1}}]/.sum[0,__]:>0;

Return[Fequ~Join~Hequ]
];


Clear@AutoEqu$GenerateEqu$SignedProj;
Options[AutoEqu$GenerateEqu$SignedProj]={"OPERules"->{}};
AutoEqu$GenerateEqu$SignedProj[signedprojobj_,OpsDef_,vacrep_,corlist_List,OptionsPattern[]]:=Module[
{OPERules,getOPErule},

OPERules=OptionValue["OPERules"];
getOPErule[cor_]:=If[#===cor,{},#]&@(cor/.OPERules);

(AutoEqu$GenerateEqu$SingleCorrelator[signedprojobj,OpsDef,vacrep,#]/.getOPErule[#]
)&/@corlist//Flatten//(#/.getOPErule["All"])&//AutoEqu$SortOPECoeff[signedprojobj,#]&
];

AutoEqu$GenerateEqu[projobj_,OpsDef_,vacrep_,corlist_List,options___]:=Module[
{signedprojobj},
Print["Warning: AutoEqu only support \[Delta] tensor structure. For other tensor structures (say \!\(\*SubscriptBox[\(Q\), \(ijkl\)]\) in cubic), AutoEqu$MMatrixFromProjs has to be modified.
2020/07/03 : simple cases involves \[Epsilon] is also supported. But there might be issue when both \[Epsilon] \[Delta] appear in same term. For more general cases involves \[Epsilon], AutoEqu$MMatrix, AutoEqu$MMatrixFromProjs need to be carefully tested."];

If[ObjGet[projobj,"RepSign",Default->"Missing"]==="Missing",
AutoEqu$SignedProjObj=signedprojobj=AutoEqu$SignReps[projobj],
AutoEqu$SignedProjObj=signedprojobj=projobj
];

AutoEqu$GenerateEqu$SignedProj[signedprojobj,OpsDef,vacrep,corlist,options]
];


Clear@AutoEqu$Sort\[Lambda]12\[ScriptCapitalO];
AutoEqu$Sort\[Lambda]12\[ScriptCapitalO][signedprojobj_,\[Beta][f1:op[op1_,rep1_,1,ss1_],f2:op[op2_,rep2_,1,ss2_],f3:op[op,rep3_,1,ss3_]][n_]]:=Module[
{repsignobj,sign},
If[OrderedQ[{f1,f2}],\[Beta][f1,f2,f3][n]//Return];

If[rep1=!=rep2,ss3 \[Beta][f2,f1,f3][n]//Return];

repsignobj=ObjGet[signedprojobj,"RepSign",Default->"Missing"];
If[repsignobj==="Missing",Print["RepSign member is not present in signedprojobj object:",signedprojobj];Abort[];];

sign= ObjGet[repsignobj,rep1,rep3];

If[sign==0,Print["logic error : impossible case : sign=0 . {rep1,rep2,rep3}=",{rep1,rep2,rep3}]];

sign ss3 \[Beta][f2,f1,f3][n]//Return;
];

(* for external operators. HAVE BUG!! *)
AutoEqu$Sort\[Lambda]12\[ScriptCapitalO][signedprojobj_,\[Beta][f1:op[op1_/;op1=!=op,rep1_,1,1],f2:op[op2_/;op2=!=op,rep2_,1,1],f3:op[op3_/;op3=!=op,rep3_,1,1]][n_]]:=Module[
{},
\[Beta][Sequence@@Sort[{f1,f2,f3}]][n]
];

AutoEqu$SortOPECoeff[signedprojobj_,expr_]:=Module[
{},
expr/.ope:\[Beta][__][_]:>AutoEqu$Sort\[Lambda]12\[ScriptCapitalO][signedprojobj,ope]//.sum[c_?NumericQ A_,rest__]:>c sum[A,rest]/.single[0]->0
];


AutoEqu$SpinSelection[signedprojobj_,opslist_,replist_,midrep_]:=Module[
{repsignobj,spinL,spinR,spinsign,getspin,
midrep$name,midrep$leftindex,midrep$rightindex,rep$purename,rep$CC
},

{midrep$name,midrep$leftindex,midrep$rightindex,rep$purename,rep$CC}=ParseRep[midrep];

repsignobj=ObjGet[signedprojobj,"RepSign",Default->"Missing"];
If[repsignobj==="Missing",Print["RepSign member is not present in signedprojobj object:",signedprojobj];Abort[];];

getspin[{op1_,op2_},{rep1_,rep2_},rep3_,rep3$index_]:=Module[
{rslt},
If[op1=!=op2,Return[0]];
If[rep1=!=rep2,Return[0]];
rslt=ObjGet[repsignobj,rep1,rep3<>"["<>ToString[rep3$index]<>"]",Default->"Missing"];
If[rslt==="Missing",
If[rep3$index===1,
rslt=ObjGet[repsignobj,rep1,rep3],
Print["getspin : ",{{op1,op2},{rep1,rep2},rep3,rep3$index}];
Print[rep3<>" is not found in signedprojobj object:"];
Abort[];
]
];
rslt
];

spinL=getspin[opslist[[1;;2]],replist[[1;;2]],midrep$name,midrep$leftindex];
spinR=getspin[opslist[[3;;4]],replist[[3;;4]],midrep$name,midrep$rightindex];
spinsign=Which[
spinL==0,spinR,
spinR==0,spinL,
spinL==spinR,spinR,
True,Print["RepSign error: inconsistent sign ",{spinL,spinR}," for opslist=",opslist," replist=",replist," midrep=",midrep];Abort[];
];

spinsign
];


AutoEqu$SignReps[projobj_]:=Module[
{AAoplist,repsign,pos},
AAoplist=DeleteDuplicates@Flatten@Cases[projobj,HoldPattern[Projector[cor_]->_]:>{Pair[cor[[1;;2]]],Pair[cor[[3;;4]]]},Infinity]//Cases[#,Pair[{a_,a_}]:>a]&;

repsign=Module[
{op=#,cor=Table[#,4],proj,ijkllist},
proj=AutoEqu$GetProjectors[projobj,cor];
ijkllist=AutoEqu$GenerateProjectorIndices[projobj,cor];
op->MapAt[AutoEqu$ProjectorSign[#,ijkllist]&,proj,{All,2}]
]&/@AAoplist;

Append[projobj,"RepSign"->repsign]
];


(* return {repCCname,leftindex,rightindex,repPurename,CC} *)
ParseRep[strrep_String]:=Module[
{repCCname,leftindex,rightindex,purerep,CC},

If[Not@StringMatchQ[strrep,__~~"["~~__~~","~~__~~"]"],
repCCname=strrep;
leftindex=rightindex=1;
,
{repCCname,leftindex,rightindex}=If[Length@#=!=3,Print["Invalid rep name=",strrep];Abort[];,#]&@StringSplit[strrep,{"[",",","]"}];
leftindex=ToExpression[leftindex];
rightindex=ToExpression[rightindex];
];

Which[
StringMatchQ[repCCname,__~~"$e"],
purerep=StringReplace[repCCname,"$e":>""];CC=1;
,
StringMatchQ[repCCname,__~~"$o"],
purerep=StringReplace[repCCname,"$o":>""];CC=-1;
,
True,
purerep=repCCname;CC=1;
];

{repCCname,leftindex,rightindex,purerep,CC}
];
ParseRep$PureName[strrep_String]:=ParseRep[strrep][[4]];
ParseRep$CCName[strrep_String]:=ParseRep[strrep][[1]];


AutoEqu$GetRepDim[projobj_,rep_]:=Module[
{dimobj,repdim},
dimobj=ObjGet[projobj,"RepDim"];
AutoEqu$RepDimObj$GetDim[dimobj,rep]
];

AutoEqu$RepDimObj$GetDim[dimobj_,rep_]:=Module[
{repdim},
repdim=ObjGet[dimobj,ParseRep$PureName[rep],Default->"Missing"];
If[repdim==="Missing",Print["dimension information misses in RepDim. rep=",rep];Abort[];];
repdim
];


Clear@AutoEqu$Projector$PPP2CN;
AutoEqu$Projector$PPP2CN[projobj_]:=Module[
{pos},
pos=Join[#,{2,All}]&/@Position[projobj,Projector][[All,1;;-3]];

MapAt[Module[{repname=#[[1]],repdim},
repdim=AutoEqu$GetRepDim[projobj,repname];
If[repdim==="Missing",Print["dimension information misses in RepDim. rep=",repname];Abort[];];

With[{invsqrtdim=1/Sqrt[repdim],projfunc=#[[2]]},
repname->(invsqrtdim projfunc[##]&)]
]&,projobj,pos]
];


AutoEqu$GenerateEqu$ProductProjector[projobjlist_,projlabel_]:=Module[
{projlist,projlist$withparm,rslt},

projlist$withparm=MapThread[AutoEqu$GetProjectors[#1,#2]&,{projobjlist,Transpose[projlabel]}];

rslt=Outer[
Module[{reps,projs},
{reps,projs}=Transpose[{##}/.Rule->List];
StringRiffle[reps,","]->ProductProjector[projs]
]&,Sequence@@projlist$withparm];

rslt//Flatten
];

Clear@AutoEqu$GenerateEqu$ProdGroup;
AutoEqu$GenerateEqu$ProdGroup[projobjlist_,OpsDef_,vacrep_,corlist_List]:=Module[
{prodprojobj},
prodprojobj=AutoEqu$GenerateProdProjectorObject[projobjlist,OpsDef,corlist];
AutoEqu$GenerateEqu[prodprojobj,OpsDef,vacrep,corlist]
];


ProductProjector[projs_List][is_,js_,ks_,ls_]:=Times@@MapThread[Apply,{projs,Transpose[{is,js,ks,ls}]}];


(* AutoEqu$MMatrixV2 only works for projectors in canonical convention!! *)
AutoEqu$MMatrixV2[projobj_,correlator_]:=Module[
{existingdata,dprojs,eprojs,Mmatrix,ijkllist,d3434proj,dprojs$dual},

existingdata=ObjGet[projobj,"MMatrix",Default->"Missing"];
If[existingdata=!="Missing",
existingdata=ObjGet[existingdata,HoldPattern[correlator],Default->"Missing"];
If[existingdata=!="Missing",Return[existingdata]];
];

dprojs=AutoEqu$GetProjectors[projobj,correlator];
eprojs=AutoEqu$GetProjectors[projobj,correlator[[{3,2,1,4}]]];
d3434proj=AutoEqu$GetProjectors[projobj,correlator[[{3,4,3,4}]]];
dprojs$dual=Module[{rep=#[[1]],proj},
proj=rep/.d3434proj;
If[proj===rep,Print["AutoEqu$MMatrixV2 logic error: I can't find rep=",rep," in d3434proj. This can't be true!"];Abort[];];
rep->proj
]&/@dprojs;

ijkllist=AutoEqu$GenerateProjectorIndices[projobj,correlator];

Mmatrix=AutoEqu$MMatrixFromProjsV2[projobj,correlator,dprojs,eprojs,dprojs$dual];

{Mmatrix,dprojs[[All,1]],eprojs[[All,1]]}
];

(* give M defined by Subscript[Q, R]=\!\(
\*SubscriptBox[\(\[Sum]\), \(S\)]\(
\*SubscriptBox[\(M\), \(R\ S\)]
\*SubscriptBox[\(P\), \(S\)]\)\) *)
AutoEqu$MMatrixFromProjsV2[projobj_,correlator_,Dprojs_List,Eprojs_List,Dprojs$dual_List]:=Module[
{DprojN=Length@Dprojs,ilist,jlist,klist,llist,mlist,nlist,MRS},
{ilist,jlist,klist,llist}=AutoEqu$GenerateProjectorIndices[projobj,correlator];
{mlist,nlist}=AutoEqu$GenerateProjectorIndices[projobj,correlator[[{3,4,3,4}]]][[3;;4]];

MRS=Table[Module[{Eproj=Eprojs[[R,2]],Dproj=Dprojs[[S,2]],Dproj$dual=Dprojs$dual[[S,2]],coeff},
coeff=Sqrt[AutoEqu$GetRepDim[projobj,Dprojs[[S,1]]]] (Eproj[klist,jlist,ilist,llist]Dproj$dual[klist,llist,mlist,nlist]//Simplify\[Delta])/(Dproj[ilist,jlist,mlist,nlist]//Simplify\[Delta])//Cancel;
coeff
],{R,1,Length@Eprojs},{S,1,Length@Dprojs}];

Return[MRS];
];


Clear@AutoEqu$GenerateProdProjectorObject;
AutoEqu$GenerateProdProjectorObject[projobjlist_,OpsDef_,corlist_List]:=Module[
{prodrepname,prodrepnamesplit,op2rep,projlabels,prodprojlist,IndexTypeRuleList,prodIndexTypeList,prodprojobj,allreps,repdim,repdimobjlist,getrepdim,prodMobj},
prodrepname=StringRiffle[#,","]&;
prodrepnamesplit=StringSplit[#,","]&;

(* calculate projectors of product group *)

op2rep=Rule@@@MapAt[StringSplit[#,","]&,OpsDef,{All,2}];
projlabels=(Join[AutoEqu$Exchange31/@#,#])&@(corlist/.op2rep)//DeleteDuplicates;

prodprojlist=Projector[prodrepname/@#]->AutoEqu$GenerateEqu$ProductProjector[projobjlist,#]&/@projlabels;

(* calculate index type of product group *)

IndexTypeRuleList=Cases[#,HoldPattern[IndexType[rep_String]->t_]:>(rep->t)]&/@projobjlist;

prodIndexTypeList=Module[{},
IndexType[prodrepname[#]]->MapThread[#2/.#1&,{IndexTypeRuleList,#}]
]&/@op2rep[[All,2]];

(* calculate dimension of product reps *)

allreps=prodprojlist[[All,2,All,1]]//Flatten//DeleteDuplicates;
allreps=prodrepnamesplit/@allreps;

repdimobjlist=ObjGet[#,"RepDim"]&/@projobjlist;
getrepdim[replist_List]:=MapThread[ObjGet[#1,#2]&,{repdimobjlist,replist}];
repdim=prodrepname@#->Times@@getrepdim@#&/@allreps;

(* calculate M matrix *)
Print["Warning: Projectors must be in canonical convention! Otherwise ProdMMatrix is not correct."];

prodMobj=Module[{reps4pt=#,MMatrixlist,MMatrix,Dreps,Ereps},
MMatrixlist=MapThread[Module[{prodobj=#1,cor=#2},
AutoEqu$MMatrixV2[prodobj,cor]
]&,{projobjlist,Transpose[reps4pt]}];

MMatrix=ArrayFlatten[TensorProduct@@#]&@MMatrixlist[[All,1]];
Dreps=Flatten@Outer[prodrepname[{##}]&,Sequence@@MMatrixlist[[All,2]]];
Ereps=Flatten@Outer[prodrepname[{##}]&,Sequence@@MMatrixlist[[All,3]]];

(prodrepname/@reps4pt)->{MMatrix,Dreps,Ereps}
]&/@projlabels;

(* assemble the final object *)

prodprojobj=Join[prodprojlist,prodIndexTypeList,{"RepDim"->repdim,"MMatrix"->prodMobj}];

prodprojobj
];


(*   Subscript[Q, R]=\!\(
\*SubscriptBox[\(\[Sum]\), \(S\)]\(
\*SubscriptBox[\(M\), \(R\ S\)]
\*SubscriptBox[\(P\), \(S\)]\)\) ;  \!\(
\*SubsuperscriptBox[\(M\), \(R\ S\), \((CN)\)]\  = \ \(
\*FractionBox[\(1\), 
SqrtBox[\(dim(R)\)]]
\*SubsuperscriptBox[\(M\), \(R\ S\), \((PPP)\)]
\*SqrtBox[\(dim(S)\)]\)\) ;
M^(CN)=M^(R).M^(PPP).M^(S) ;
Subscript[(M^(S)), S',S]=Sqrt[dim(S)] Subscript[\[Delta], S',S];
Subscript[(M^(R)), R',R]=1/Sqrt[dim(R)] Subscript[\[Delta], R',R];
  *)
MMatrix$PPP2CN[{MMatrix_,Dreps_,Ereps_},RepDim_]:=Module[
{Dreps$dim,Ereps$dim},
{Dreps$dim,Ereps$dim}={AutoEqu$RepDimObj$GetDim[RepDim,#]&/@Dreps,AutoEqu$RepDimObj$GetDim[RepDim,#]&/@Ereps};
{DiagonalMatrix[1/Sqrt[Ereps$dim]].MMatrix.DiagonalMatrix[Sqrt[Dreps$dim]],Dreps,Ereps}
];


AutoEqu$OPEDisplayForm[expr_]:=expr/.op[o_/;o=!=op,ch_,_,s_]:>o/.op[op,ch_,1,s_]:>ch<>If[s==-1,"-",""]/.\[Beta][op1_,op2_,op3_][n_]:>Subsuperscript[\[Beta],Row[{op1,op2,op3},","],Row[{"(",n,")"}]];




(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     12413,        290]
NotebookOptionsPosition[     11947,        272]
NotebookOutlinePosition[     12352,        289]
CellTagsIndexPosition[     12309,        286]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"GetFileDirectory", "[", "]"}], ":=", 
   RowBox[{"If", "[", 
    RowBox[{
     RowBox[{"$InputFileName", "===", "\"\<\>\""}], ",", 
     RowBox[{"NotebookDirectory", "[", "]"}], ",", 
     RowBox[{"DirectoryName", "@", "$InputFileName"}]}], "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"AppendTo", "[", 
   RowBox[{"$Path", ",", 
    RowBox[{"GetFileDirectory", "[", "]"}]}], "]"}], ";"}]}], "Code",Expressio\
nUUID->"37da522e-b13c-4bde-8b95-ea6b9465e9ec"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"FromBase64", "[", "str_String", "]"}], ":=", 
   RowBox[{
    RowBox[{
     RowBox[{"If", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"Head", "[", "#", "]"}], "===", "String"}], ",", 
       RowBox[{"ToExpression", "@", "#"}], ",", "#"}], "]"}], "&"}], "@", 
    RowBox[{"ImportString", "[", 
     RowBox[{"str", ",", 
      RowBox[{"{", 
       RowBox[{"\"\<Base64\>\"", ",", "\"\<String\>\""}], "}"}]}], "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Print$stdout$term", "[", "str_String", "]"}], ":=", 
   RowBox[{"WriteString", "[", 
    RowBox[{"(*", "\"\<stdout\>\"", "*)"}], 
    RowBox[{"$Output", ",", "str"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Print$stdout", "[", "arg__", "]"}], ":=", 
    RowBox[{
     RowBox[{
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"Head", "[", "#", "]"}], "===", "String"}], ",", 
        RowBox[{"Print$stdout$term", "[", "#", "]"}], ",", 
        RowBox[{"Print$stdout$term", "[", 
         RowBox[{"ToString", "[", 
          RowBox[{"#", ",", "StandardForm"}], "]"}], "]"}]}], "]"}], "&"}], "/@", 
     RowBox[{"{", 
      RowBox[{"arg", ",", "\"\<\\n\>\""}], "}"}]}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Bootstrapper$boot", "[", "]"}], ":=", 
    RowBox[{"Module", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
       "detectcmd", ",", "param", ",", "\[CapitalDelta]0val", ",", 
        "\[CapitalDelta]1val", ",", "sdpdata", ",", "rslt"}], "}"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"detectcmd", "=", 
        RowBox[{"$CommandLine", "[", 
         RowBox[{"[", 
          RowBox[{"-", "1"}], "]"}], "]"}]}], ";", "\n", 
       "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{"detectcmd", "===", "\"\<test\>\""}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"param", "=", 
           RowBox[{"FromBase64", "@", 
            RowBox[{"$CommandLine", "[", 
             RowBox[{"[", 
              RowBox[{"-", "2"}], "]"}], "]"}]}]}], ";", 
          "\[IndentingNewLine]", 
          RowBox[{"Print", "[", "param", "]"}], ";", "\[IndentingNewLine]", 
          RowBox[{"Print", "[", 
           RowBox[{"$CommandLine", "[", 
            RowBox[{"[", 
             RowBox[{"-", "2"}], "]"}], "]"}], "]"}], ";", 
          "\[IndentingNewLine]", "\[IndentingNewLine]", 
          RowBox[{"Return", "[", "]"}], ";"}]}], "\[IndentingNewLine]", "]"}],
        ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{"detectcmd", "===", "\"\<evaluate\>\""}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"Module", "[", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{"filename", ",", "expr"}], "}"}], ",", 
            "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"param", "=", 
              RowBox[{"FromBase64", "@", 
               RowBox[{"$CommandLine", "[", 
                RowBox[{"[", 
                 RowBox[{"-", "2"}], "]"}], "]"}]}]}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"Simpleboot$MainFile", "=", 
              RowBox[{"filename", "=", 
               RowBox[{"param", "[", 
                RowBox[{"[", "1", "]"}], "]"}]}]}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"expr", "=", 
              RowBox[{"param", "[", 
               RowBox[{"[", "2", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
             RowBox[{"Print$stdout", "[", 
              RowBox[{
              "\"\<SSH$Evaluate: filename=\>\"", ",", "filename", ",", 
               "\"\<, expr=\>\"", ",", "expr", ",", 
               "\"\<. Start evaluating...\>\""}], "]"}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"Get", "[", "filename", "]"}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"rslt", "=", 
              RowBox[{"expr", "/.", 
               RowBox[{"Hold", "\[Rule]", "Identity"}]}]}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"Print$stdout", "[", 
              RowBox[{"\"\<[RemoteExecuteReturnBegin]\>\"", "<>", 
               RowBox[{"ToBase64", "@", "rslt"}], "<>", 
               "\"\<[RemoteExecuteReturnEnd]\>\""}], "]"}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"Print$stdout", "[", 
              RowBox[{"\"\<SSH$Evaluate result:\\n\>\"", ",", "rslt"}], "]"}],
              ";"}]}], "\[IndentingNewLine]", "]"}], ";", 
          "\[IndentingNewLine]", 
          RowBox[{"Return", "[", "1", "]"}], ";"}]}], "\[IndentingNewLine]", 
        "]"}], ";", "\n", "\n", 
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{"detectcmd", "===", "\"\<RemoteExecute\>\""}], ",", "\n", 
         RowBox[{
          RowBox[{"Module", "[", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{"filename", ",", "expr"}], "}"}], ",", "\n", 
            RowBox[{
             RowBox[{"param", "=", 
              RowBox[{"FromBase64", "@", 
               RowBox[{"$CommandLine", "[", 
                RowBox[{"[", 
                 RowBox[{"-", "2"}], "]"}], "]"}]}]}], ";", "\n", 
             RowBox[{"filename", "=", 
              RowBox[{"param", "[", 
               RowBox[{"[", "1", "]"}], "]"}]}], ";", "\n", 
             RowBox[{"expr", "=", 
              RowBox[{"param", "[", 
               RowBox[{"[", "2", "]"}], "]"}]}], ";", "\n", "\n", 
             RowBox[{"Get", "[", "filename", "]"}], ";", "\n", 
             RowBox[{"rslt", "=", 
              RowBox[{"expr", "//.", 
               RowBox[{"Hold", "->", "Identity"}]}]}], ";", "\n", "\n", 
             RowBox[{"Print$stdout", "[", 
              RowBox[{"\"\<[RemoteExecuteReturnBegin]\>\"", "<>", 
               RowBox[{"ToBase64", "@", "rslt"}], "<>", 
               "\"\<[RemoteExecuteReturnEnd]\>\""}], "]"}], ";"}]}], "\n", 
           "]"}], ";", "\n", 
          RowBox[{"Return", "[", "1", "]"}], ";"}]}], "\n", "]"}], ";", "\n", 
       "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{"detectcmd", "===", "\"\<FeasibilityPrint\>\""}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"Module", "[", 
           RowBox[{
            RowBox[{"{", 
             RowBox[{
             "filename", ",", "SDPGenerator", ",", "point", ",", "SDPBopt", 
              ",", "bFA", ",", "rsltInterpreter", ",", "FeasibilityRslt"}], 
             "}"}], ",", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"param", "=", 
              RowBox[{"FromBase64", "@", 
               RowBox[{"$CommandLine", "[", 
                RowBox[{"[", 
                 RowBox[{"-", "2"}], "]"}], "]"}]}]}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{
              RowBox[{"{", 
               RowBox[{
               "filename", ",", "SDPGenerator", ",", "point", ",", "SDPBopt", 
                ",", "rsltInterpreter", ",", "bFA"}], "}"}], "=", "param"}], 
             ";", "\[IndentingNewLine]", 
             RowBox[{"Get", "[", "filename", "]"}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"LocalSDPBFeasiblity$DoFunctionalAnalysis", "=", "bFA"}],
              ";", "\[IndentingNewLine]", 
             RowBox[{"FeasibilityRslt", "=", 
              RowBox[{"LocalSDPBFeasiblity", "[", 
               RowBox[{
               "SDPGenerator", ",", "point", ",", "SDPBopt", ",", 
                "rsltInterpreter"}], "]"}]}], ";", "\[IndentingNewLine]", 
             RowBox[{"Print$stdout", "[", 
              RowBox[{"\"\<[FeasibilityReturnBegin]\>\"", "<>", 
               RowBox[{"ToBase64", "@", 
                RowBox[{"FeasibilityRslt", "[", 
                 RowBox[{"[", "1", "]"}], "]"}]}], "<>", 
               "\"\<[FeasibilityReturnEnd]\>\""}], "]"}], ";", 
             "\[IndentingNewLine]", 
             RowBox[{"Print$stdout", "[", 
              RowBox[{"FeasibilityRslt", "[", 
               RowBox[{"[", "2", "]"}], "]"}], "]"}], ";"}]}], 
           "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
          RowBox[{"Return", "[", "1", "]"}], ";"}]}], "\[IndentingNewLine]", 
        "]"}], ";"}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", "]"}]}],
    ";"}], "\n"}], "\n", 
 RowBox[{
  RowBox[{"Bootstrapper$boot", "[", "]"}], ";"}]}], "Code",
 CellChangeTimes->{{3.6696896992554836`*^9, 3.6696897019283805`*^9}, {
   3.669690568674554*^9, 3.669690632647369*^9}, {3.669690761842989*^9, 
   3.6696907620743537`*^9}, {3.6696907975941772`*^9, 
   3.6696908008165746`*^9}, {3.669691458200699*^9, 3.669691463246633*^9}, {
   3.669691623088888*^9, 3.669691646574668*^9}, {3.6696932540599427`*^9, 
   3.669693327867112*^9}, 3.6696933733038654`*^9, {3.6696940236978426`*^9, 
   3.669694046860795*^9}, 3.669727663448698*^9, {3.6697277152781067`*^9, 
   3.6697277156941457`*^9}, {3.6697277760583057`*^9, 3.669727885020643*^9}, {
   3.669729040813369*^9, 3.669729062316453*^9}, {3.669729514739879*^9, 
   3.6697295262745085`*^9}, 3.669734181572507*^9, {3.6697494884344716`*^9, 
   3.6697495067410107`*^9}, {3.669749711537495*^9, 3.669749865351445*^9}, {
   3.6697504918029327`*^9, 3.6697505056008253`*^9}, {3.6697506422397275`*^9, 
   3.6697506492411346`*^9}, {3.669750827583644*^9, 3.6697509262313824`*^9}, {
   3.669753048449769*^9, 3.669753090018734*^9}, {3.6697531438922205`*^9, 
   3.669753163898213*^9}, {3.6697532031357145`*^9, 3.6697532348311005`*^9}, {
   3.6697533032634196`*^9, 3.6697533398737345`*^9}, {3.6697547585076547`*^9, 
   3.6697547788482456`*^9}, {3.669754822448247*^9, 3.669754891796545*^9}, {
   3.6697618218567915`*^9, 3.669761961050294*^9}, {3.6697619942045436`*^9, 
   3.669762176594318*^9}, 3.6697622109877343`*^9, 3.669762323687025*^9, {
   3.6697625914666014`*^9, 3.6697625941164265`*^9}, 3.669762758926387*^9, {
   3.6697628462247314`*^9, 3.669762858328722*^9}, {3.669762959395339*^9, 
   3.6697629620997014`*^9}, {3.6697631615174894`*^9, 3.669763161871168*^9}, {
   3.6697667888847027`*^9, 3.6697669710514507`*^9}, {3.6697671010898595`*^9, 
   3.669767108490503*^9}, 3.6698310994054236`*^9, {3.6698337983045206`*^9, 
   3.6698338379903374`*^9}, {3.705671750163103*^9, 3.7056717579636707`*^9}, {
   3.7056720702311535`*^9, 3.7056720795144815`*^9}, {3.7263716287238197`*^9, 
   3.7263716355962815`*^9}, {3.7499030084468875`*^9, 
   3.7499030130496497`*^9}, {3.757186910417119*^9, 3.7571869171172376`*^9}, {
   3.75734768797952*^9, 3.7573476990529537`*^9}, {3.7573479767351103`*^9, 
   3.757347980489111*^9}, {3.912549406129283*^9, 
   3.9125494118284407`*^9}},ExpressionUUID->"a412b3b3-41c3-46ab-95b7-\
97bb5c96980c"]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1280, 596},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
Magnification:>1.4 Inherited,
FrontEndVersion->"11.1 for Microsoft Windows (64-bit) (May 16, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 502, 13, 96, "Code", "ExpressionUUID" -> \
"37da522e-b13c-4bde-8b95-ea6b9465e9ec"],
Cell[1063, 35, 10880, 235, 1546, "Code", "ExpressionUUID" -> \
"a412b3b3-41c3-46ab-95b7-97bb5c96980c"]
}
]
*)


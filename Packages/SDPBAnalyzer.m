(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



TranslatePDLength[conf$pd_]:=Module[
{},
Length[#[[1]]]&/@conf$pd
];
TranslatePDIndex[conf$pd_,index_List]:=Module[
{pdlen},
pdlen=TranslatePDLength[conf$pd];
Range[Plus@@pdlen[[1;;#-1]]+1,Plus@@pdlen[[1;;#-1]]+ pdlen[[#]]]&/@index//Flatten
];
TranslatePDIndex[conf$pd_,index_Integer]:=TranslatePDIndex[conf$pd,{index}];
(* only accept Span of the form i;;k, or ;; *)
TranslatePDIndex[conf$pd_,index_Span]:=Module[
{pdlen,first,last},
pdlen=TranslatePDLength[conf$pd];
first=Plus@@pdlen[[1;;index[[1]]-1]]+1;
last=Plus@@pdlen[[1;;index[[2]]]];
first;;last
];


GetzFromsdpb$OldSDPB[outFile_,normalization_]:=Module[
{ytable,ztable},
Get[outFile];
ytable=y/.{a_ e+b_:>a 10^b};

ztable=dereshuffleWithNormalization$yToz[normalization,ytable];

Clear[x,y];

ztable
];


GetzFromsdpb$SDPBuy[outFile_,normalization_]:=Module[
{ytable,ztable},
Get[outFile];
ytable=y/.{a_ e+b_:>a 10^b};

Clear[x,y];

ytable
];


(* --------------- for SDPB 2.0.4 , obsolete --------------- *)
SDPBELOutFileParser$xy[str_String]:=ToExpression/@StringSplit[str,{"\n","{","}"}];
SDPBELOutFileParser$xy[str_String]:=ToExpression[str];
SDPBELOutFileParser[outtxt_String]:=Module[
{},
StringCases[outtxt,
StartOfLine~~Shortest[term__]~~Whitespace~~"="~~Whitespace~~Shortest[value__]~~";":>Switch[term,
"x"|"y",(term->SDPBELOutFileParser$xy@StringReplace[value,{"e+":>"*^","e-":>"*^-"}]),
_,(term->ToExpression@StringReplace[value,{"e+":>"*^","e-":>"*^-"}])
]
]
];
GetzFromsdpb$SDPBEL["2.0.4",outFile_,normalization_]:=Module[
{outtext,ytable,ztable},
outtext=Import[outFile,"Text"];

ytable="y"/.SDPBELOutFileParser[outtext];

ztable=dereshuffleWithNormalization$yToz[normalization,ytable];

ztable
];
(* --------------------------------------------------------- *)



GetzFromsdpb$SDPBEL["2.1.2",outFile_,normalization_]:=Module[
{ytxt,ytable,ztable},
ytxt=FileNameJoin[{outFile,"y.txt"}];
ytable=LoadExpressionList[ytxt][[2;;-1]]/.{a_ e+b_:>a 10^b};
ztable=dereshuffleWithNormalization$yToz[normalization,ytable];

ztable
];

GetzFromsdpb[outFile_,normalization_]:=Switch[Cluster$GetConfig["[SDPB.Version]"],
"sdpb",GetzFromsdpb$OldSDPB[outFile,normalization],
"sdpb_El",GetzFromsdpb$SDPBEL["2.1.2",outFile,normalization],
"sdpb_uy",GetzFromsdpb$SDPBuy[outFile,normalization],
_,Print["GetzFromsdpb error: unsupported sdpb version :",Cluster$GetConfig["[SDPB.Version]"]];
];

dereshuffleWithNormalization$yToz[normalization_, y_] := Module[
{j = maxIndexBy[normalization, Abs], z,zj,b},
z=y;
z=Insert[z,0,j];
zj=(1-Sum[normalization[[sum$i]]z[[sum$i]],{sum$i,1,Length[z]}])/normalization[[j]];
z=ReplacePart[z,j->zj];

z
];

AnalyzeSDPBResult$zW[outFile_,conf$pd_,normalization_,W_MatricesBlock,sumover_:;;]:=Module[
{index},
index=TranslatePDIndex[conf$pd,sumover];
GetzFromsdpb[outFile,normalization][[index]].W[[2]][[index]]//Expand
];

AnalyzeSDPBResult$zn[outFile_,conf$pd_,normalization_,sumover_:;;]:=Module[
{index},
index=TranslatePDIndex[conf$pd,sumover];
normalization[[index]].GetzFromsdpb[outFile,normalization][[index]]
];


(* Note 2020/02/17 : Sometime Solve stuck forever. Somehow \!\(
\*SubscriptBox[\(\[PartialD]\), \(x\)]expr\) make Solve stuck, but ToExpression@ToString[\!\(
\*SubscriptBox[\(\[PartialD]\), \(x\)]expr\),InputForm] do not, although they are exactly same (===)
Note 2020/11/09 : Somehow Solve still stuck for some cases. the fix is change 0 to 0.  . This fix also works for the bad case before.
Note 2020/11/16 : The previous fix still don't work for some cases  . The new fix works for all known bad cases.
Note 2020/11/18 : The issue seems related to WorkingPrecision. The new fix works for all known bad cases.

Note 2021/5/21 : I conclude that MMA always have some bugs related to Solve polynomial with high precision high degree. Sometimes it stuck sometimes it just random segmentation fault. This new version provide an option to use MPSolve
   *)
AnalysisSDPBOut$FindMin[expr_/;FreeQ[expr,x]]:={Any,expr,{}};

(*
AnalysisSDPBOut$FindMin[expr_]:=Module[
{sol,sol2,minima,minx,miny},

(*sol=Solve[ToExpression@ToString[\!\(
\*SubscriptBox[\(\[PartialD]\), \(x\)]expr\),InputForm]\[Equal]0,{x},Reals]//Cases[#,{x\[Rule]a_}/;a\[GreaterEqual]0\[RuleDelayed]a]&;*)
sol=NSolve[\!\(
\*SubscriptBox[\(\[PartialD]\), \(x\)]expr\),{x}]//Cases[#,{x\[Rule]a_}/;Head[a]=!=Complex]&//Cases[#,{x\[Rule]a_}/;a\[GreaterEqual]0\[RuleDelayed]a]&;

minima=Select[sol,(\!\(
\*SubscriptBox[\(\[PartialD]\), \({x, 2}\)]expr\)/.x\[Rule]#)>0&];

If[Length@minima\[Equal]0,Return[{Zero,expr/.x\[Rule]0,{}}]];

(*minx=MinimalBy[sol,(expr/.x\[Rule]#)&]//SNAssertC[Length@#\[Equal]1&]//#[[1]]&;*)

sol2=sol~Join~{0};
minx=MyMinimalBy[sol2,(expr/.x\[Rule]#)&];
miny=expr/.x\[Rule]minx;

If[minx\[Equal]0,minx=ZERO];
{minx,miny,minima}
];*)

AnalysisSDPBOut$FindMin[expr_]:=Module[
{sol,sol2,minima,minx,miny,pdxpoly},
If[SBConfig$mpsolveQ,
sol=SolvePolynomialRoot$MPSolve$Real[\!\(
\*SubscriptBox[\(\[PartialD]\), \(x\)]expr\)];
,
sol=SolvePolynomialRoot$MMA$Real[\!\(
\*SubscriptBox[\(\[PartialD]\), \(x\)]expr\)];
];
sol=Select[sol,#>=0&];

minima=Select[sol,(\!\(
\*SubscriptBox[\(\[PartialD]\), \({x, 2}\)]expr\)/.x->#)>0&];

If[Length@minima==0,Return[{Zero,expr/.x->0,{}}]];

sol2=sol~Join~{0};
minx=MyMinimalBy[sol2,(expr/.x->#)&];
miny=expr/.x->minx;

If[minx==0,minx=ZERO];
{minx,miny,minima}
];


(* Format:
SDPData[BC$b_,BC$n_,BC$M_,{{x_,y_},conf$pd_,lset_,\[CapitalLambda]max_}]
*)
SDPBReport=AnalysisSDPBOut;

Clear@AnalysisSDPBOut;
AnalysisSDPBOut[outfile_String,sdpinfo_SDPData]:=Module[
{outFile2,conf$pd,BC$b,BC$n,BC$M,Nconditions,z,z$Row,n$Row,rowN,zW$Row,zW,zW$Det,zWmin,obj},

BC$n=sdpinfo[[2]];

z=GetzFromsdpb[FullPath$SDPBFiles$WithSuffix[outfile,".out"],BC$n];

AnalysisSDPBOut[z,sdpinfo]
];

AnalysisSDPBOut[z_List,sdpinfo_SDPData]:=Module[
{outFile2,conf$pd,BC$b,BC$n,BC$M,Nconditions,z$Row,n$Row,rowN,zW$Row,zW,zW$Det,zWmin,obj},

BC$b=sdpinfo[[1]];
BC$n=sdpinfo[[2]];
BC$M=sdpinfo[[3]];
conf$pd=sdpinfo[[4,2]];

If[Length@z=!=Length@BC$n,Print["AnalysisSDPBOut error: incorrect z dimension: dim@z=",Length@z," dim@n=",Length@BC$n];Return[Error]];

Nconditions=Length@BC$M;

rowN=Length@conf$pd;

z$Row={Module[{rowpds},
rowpds=TranslatePDIndex[conf$pd,#];
z[[rowpds]]
]}&/@Range[1,rowN];

n$Row={Module[{rowpds},
rowpds=TranslatePDIndex[conf$pd,#];
BC$n[[rowpds]]
]}&/@Range[1,rowN];

zW$Row=Module[{mb=#},
{Module[{rowpds},
rowpds=TranslatePDIndex[conf$pd,#];
z[[rowpds]].mb[[2]][[rowpds]]//Expand
]}&/@Range[1,rowN]
]&/@BC$M;

zW=(Plus@@#[[;;,1]]//Expand)&/@zW$Row;
zW$Det=Det/@zW//Expand;

zWmin=AnalysisSDPBOut$FindMin/@zW$Det;

obj=z.BC$b;

SDPBFunctionalReport[
{z,z$Row,BC$n,n$Row,zW$Row,zW,zW$Det,zWmin},
{Nconditions,sdpinfo[[4,1]],sdpinfo[[4,3]],sdpinfo[[4,4]]},
{BC$b,obj}
]
];

MyMinimalBy[list_List,f_]:=list[[Ordering[f/@list//SetPrec,1][[1]]]];

Clear[ReadSDPB];
ReadSDPB[report_]["z"]:=report[[1,1]];
ReadSDPB[report_]["z",n_]:=report[[1,2,n,1]];
ReadSDPB[report_]["n"]:=report[[1,3]];
ReadSDPB[report_]["n",n_]:=report[[1,4,n,1]];
ReadSDPB[report_]["zW",n_][l_]:=report[[1,5,l,n,1]];
ReadSDPB[report_]["zW"][l_]:=report[[1,6,l]];
ReadSDPB[report_]["det"][l_]:=report[[1,7,l]];
ReadSDPB[report_]["min"][l_]:=report[[1,8,l]];

ReadSDPB[report_]["J"]:=report[[2,1]];
ReadSDPB[report_]["pt"]:=report[[2,2]];
ReadSDPB[report_]["lset"]:=report[[2,3]];
ReadSDPB[report_]["\[CapitalLambda]max"]:=report[[2,4]];


ReadSDPB[report_]["b"]:=report[[3,1]];
ReadSDPB[report_]["obj"]:=report[[3,2]];

ReadSDPB[report_]["kerPD",n_:1]:=ReconstructFunctionalKernel$Der[ReadSDPB[report]["z",n],ReadSDPB[report]["\[CapitalLambda]max"]];
ReadSDPB[report_]["ker",n_:1]:=ReconstructFunctionalKernel[ReadSDPB[report]["kerPD",n]];



ReconstructFunctionalKernel$Der[zmn_,\[CapitalLambda]max_]:=Module[
{cmn,TMatrix,TInverse,bkl,pdgK,bklDer,bEven,EOQ,PDS},
cmn=zmn;

bEven=EvenQ[\[CapitalLambda]max];
EOQ=If[bEven,EvenQ,OddQ];
PDS=If[bEven,PartialDerivativeSet$Even,PartialDerivativeSet$Odd];

TMatrix=Table[Module[{m=MN[[1]],n=MN[[2]],k=KL[[1]],l=KL[[2]]},
1/(m!n!k!l!) (2^(-2-k-l-m-n) (1+(-1)^(k+m)) (1+(-1)^(l+n)))/((1+k+m) (1+l+n))//SetPrec
]
,{MN,PDS[\[CapitalLambda]max]},{KL,PDS[\[CapitalLambda]max]}];
TInverse=TMatrix//Inverse;
bkl=cmn.TInverse;

bklDer=MapThread[#1->#2&,{PDS[\[CapitalLambda]max],bkl}];
pdgK[m_,n_]/;m+n>\[CapitalLambda]max:=0;
pdgK[m_,n_]/;m+n<=\[CapitalLambda]max&&EOQ[m+n]&&m>n:={m,n}/.bklDer;
pdgK[m_,n_]/;m+n<=\[CapitalLambda]max&&EOQ[m+n]&&m<n:={n,m}/.bklDer;
pdgK[m_,n_]/;m+n<=\[CapitalLambda]max&&Not@EOQ[m+n]:=0;

Table[pdgK[m,n],{m,0,\[CapitalLambda]max},{n,0,\[CapitalLambda]max}]
];


ReconstructFunctionalKernel[derTab_]:=Module[
{\[CapitalLambda]max},
\[CapitalLambda]max=Length@derTab-1;
Sum[1/(m!n!) derTab[[1+m,1+n]](z-1/2)^m (zb-1/2)^n,{m,0,\[CapitalLambda]max},{n,0,\[CapitalLambda]max}]
];


SDPBReport$Plot[rptreader_,opt_:{Axes->True,AspectRatio->1}]:=Module[
{joblog,truepts,falsepts},

joblog=LoadExpressionList[SSH$ReconfigCmd["[Cluster.ProjectDirectory]/sdpb_jobs_log.txt"]];

truepts=Select[joblog,ObjGet[#,"TerminateReason"]==="primal"&];
falsepts=Select[joblog,ObjGet[#,"TerminateReason"]==="dual"&];

Show[
Graphics[{
PointSize[Medium],Red,Tooltip[Point[ObjGet[#,"Point"]],rptreader[LoadSDPBReport[ObjGet[#,"Report"]]]]&/@falsepts,
PointSize[Medium],Black,Tooltip[Point[ObjGet[#,"Point"]],rptreader[LoadSDPBReport[ObjGet[#,"Report"]]]]&/@truepts

}],Sequence@@opt,CoordinatesToolOptions->{"DisplayFunction"->N,"CopiedValueFunction"->N}]
];


Read\[ScriptL]min[rpt_,j_,\[ScriptL]_,xshift_]:=Module[
{minima,minimacolored,globalmin$color,shift,det,globalmin,ptx,pty,ptz},
globalmin$color=LightRed;

Which[
Length@ReadSDPB[rpt]["pt"]==1,ptx=ReadSDPB[rpt]["pt"][[1]],
Length@ReadSDPB[rpt]["pt"]==2,{ptx,pty}=ReadSDPB[rpt]["pt"],
Length@ReadSDPB[rpt]["pt"]==3,{ptx,pty,ptz}=ReadSDPB[rpt]["pt"],
True,{ptx,pty,ptz}={"Unknown","Unknown","Unknown"};
];

shift=xshift//.{"ptx"->ptx,"pty"->pty,"ptz"->ptz};
If[Not@NumberQ[shift],Print["Read\[ScriptL]min: unexpected form of xshift : ",xshift,", which parsed as ",shift];];

det=ReadSDPB[rpt]["det"][j];
minima=ReadSDPB[rpt]["min"][j][[3]];
globalmin=ReadSDPB[rpt]["min"][j][[1]]/.{Zero->0,ZERO->0};

If[Length@minima===0 &&  (det/.x->0)>0,Return["None"]];
minimacolored=If[(det/.x->#)<0,
If[#===globalmin,Style[shift+#,Red],Style[shift+#,Pink]]
,shift+#]&/@minima;

If[(det/.x->0)<0,PrependTo[minimacolored,Style[shift,Gray]]];

minimacolored//N
];

ReadMCritial[rpt_]:=Module[
{J,Mcritical,info},
J=ReadSDPB[rpt]["J"];
Mcritical=Select[Range[1,J],ReadSDPB[rpt]["min"][#][[2]]<0&];
info=Module[{criticalJ=#,criticalx,firstnoncriticalx,\[Alpha]F,allsol,rbound$index,lbound,rbound},
criticalx=ReadSDPB[rpt]["min"][criticalJ][[1]]//N;
\[Alpha]F=ReadSDPB[rpt]["det"][criticalJ];
allsol=\[Alpha]F//NSolve[#,x,Reals]&//Cases[#,HoldPattern[x->sol_]:>sol,{2}]&//Sort;

rbound$index=FirstPosition[allsol,a_/;a>=criticalx];

Which[
rbound$index===Missing["NotFound"],
rbound=\[Infinity];
If[Length@allsol>=1,lbound=allsol[[-1]]//N,lbound=-\[Infinity]];,

rbound$index==={1},
rbound=allsol[[1]]//N;lbound=-\[Infinity];,

True,
lbound=allsol[[rbound$index[[1]]-1]]//N;
rbound=allsol[[rbound$index[[1]]]]//N;
];

{criticalJ,criticalx,{lbound,rbound}}
]&/@Mcritical;

Grid@info
];

Read\[ScriptL]Plot[rpt_,j_,\[ScriptL]_,xshift_,{\[CapitalDelta]min_,\[CapitalDelta]max_}]:=Module[
{det,shift,ptx,pty},

ptx=ReadSDPB[rpt]["pt"][[1]];
pty=ReadSDPB[rpt]["pt"][[2]];

shift=xshift//.{"ptx"->ptx,"pty"->pty};
If[Not@NumberQ[shift],Print["Read\[ScriptL]min: unexpected form of xshift : ",xshift,", which parsed as ",shift];];

det=ReadSDPB[rpt]["det"][j]/.x->\[CapitalDelta]-shift;
det//Plot[#,{\[CapitalDelta],\[CapitalDelta]min,\[CapitalDelta]max}]&
];




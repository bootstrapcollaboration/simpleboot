(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     13734,        304]
NotebookOptionsPosition[     13212,        283]
NotebookOutlinePosition[     13638,        301]
CellTagsIndexPosition[     13595,        298]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Includes", "Section",
 CellChangeTimes->{{3.7766786173013973`*^9, 
  3.776678620838959*^9}},ExpressionUUID->"4497a1bc-9ac3-4cd6-9082-\
1050174a670c"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Bootstrapper$Installer$Packages", "[", 
     RowBox[{
      RowBox[{"freshinstall_:", "False"}], ",", 
      RowBox[{"packagedir_:", "Automatic"}]}], "]"}], ":=", 
    RowBox[{"Module", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"chmodlist", ",", "packagedir2"}], "}"}], ",", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"packagedir2", "=", 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{"packagedir", "===", "Automatic"}], ",", 
          RowBox[{"ReconfigCmd", "@", "\"\<[Cluster.PackageDirectory]\>\""}], 
          ",", "packagedir"}], "]"}]}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"Print", "[", 
        RowBox[{
        "\"\<Bootstrapper Packages will be installed to location : \>\"", ",",
          "packagedir2"}], "]"}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{"freshinstall", ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"Print", "[", 
           RowBox[{
            RowBox[{
            "ReconfigCmd", "@", 
             "\"\<Warning : All existing files in folder \>\""}], "<>", 
            "packagedir2", "<>", "\"\< will be deleted!!!\>\""}], "]"}], ";", 
          "\[IndentingNewLine]", 
          RowBox[{"SSH2$ExecuteMultiLine", "[", 
           RowBox[{"{", 
            RowBox[{"\"\<rm -R \>\"", "<>", "packagedir2"}], "}"}], "]"}]}]}],
         "]"}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"SSH2$ExecuteMultiLine", "[", 
        RowBox[{"{", 
         RowBox[{"\"\<mkdir -p \>\"", "<>", "packagedir2"}], "}"}], "]"}], 
       ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"SSH2$UploadFile$AbsoluteDir", "[", 
           RowBox[{
            RowBox[{"\"\<[Local.PackageDirectory]/\>\"", "<>", "#1"}], ",", 
            "packagedir2"}], "]"}], "&"}], ")"}], "/@", 
        "Bootstrapper$Packages$MMAFiles"}], ";", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"SSH2$UploadFile$AbsoluteDir", "[", 
           RowBox[{
            RowBox[{"\"\<[Local.PackageDirectory]/\>\"", "<>", "#1"}], ",", 
            "packagedir2"}], "]"}], "&"}], ")"}], "/@", 
        "Bootstrapper$Packages$Executables"}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"chmodlist", "=", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"\"\<chmod +x ./\>\"", "<>", "#1"}], "&"}], ")"}], "/@", 
         "Bootstrapper$Packages$Executables"}]}], ";", "\[IndentingNewLine]", 
       RowBox[{"SSH2$ExecuteMultiLine", "[", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"\"\<cd \>\"", "<>", "packagedir2"}], ",", 
          RowBox[{"Sequence", "@@", "chmodlist"}]}], "}"}], "]"}], ";"}]}], 
     "\[IndentingNewLine]", "\[IndentingNewLine]", "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Bootstrapper$Installer$Workspace", "[", 
     RowBox[{
      RowBox[{"freshinstall_:", "False"}], ",", 
      RowBox[{"packagedir_:", "Automatic"}]}], "]"}], ":=", 
    RowBox[{"Module", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"chmodlist", ",", "packagedir2", ",", "configtxt"}], "}"}], 
      ",", "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"packagedir2", "=", 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{"packagedir", "===", "Automatic"}], ",", 
          RowBox[{"ReconfigCmd", "@", "\"\<[Cluster.PackageDirectory]\>\""}], 
          ",", "packagedir"}], "]"}]}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"Print", "[", 
        RowBox[{
        "ReconfigCmd", "@", 
         "\"\<Bootstrapper Workspace will be installed to location : \
[Cluster.WorkspaceDirectory]\>\""}], "]"}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{"freshinstall", ",", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{"Print", "[", 
           RowBox[{
           "ReconfigCmd", "@", 
            "\"\<Warning : All existing files in folder \
[Cluster.WorkspaceDirectory] will be deleted!!!\>\""}], "]"}], ";", 
          "\[IndentingNewLine]", 
          RowBox[{"SSH2$ExecuteMultiLine", "[", 
           RowBox[{"{", "\"\<rm -R [Cluster.WorkspaceDirectory]\>\"", "}"}], 
           "]"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"SSH2$ExecuteMultiLine", "[", 
        RowBox[{
        "{", "\"\<mkdir -p [Cluster.WorkspaceDirectory]/Scripts\>\"", "}"}], 
        "]"}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"(*", " ", 
        RowBox[{"search", " ", "for", " ", "all", " ", "*", 
         RowBox[{".", "sh"}], " ", "in", " ", "Scripts"}], " ", "*)"}], 
       "\[IndentingNewLine]", 
       RowBox[{"Bootstrapper$Scripts$Executables", "=", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"FileNameTake", "[", 
              RowBox[{"#", ",", 
               RowBox[{"-", "1"}]}], "]"}], "&"}], "/@", 
            RowBox[{"FileNames", "[", 
             RowBox[{"ReconfigCmd", "@", "\"\<./Scripts/*.sh\>\""}], "]"}]}], 
           ")"}], "~", "Join", "~", "Bootstrapper$Scripts$Executables"}], "//",
          "DeleteDuplicates"}]}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"SSH2$UploadFile", "[", 
           RowBox[{"\"\<./Scripts/\>\"", "<>", "#1"}], "]"}], "&"}], ")"}], "/@",
         "Bootstrapper$Scripts$Executables"}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"chmodlist", "=", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"\"\<chmod +x ./\>\"", "<>", "#1"}], "&"}], ")"}], "/@", 
         "Bootstrapper$Scripts$Executables"}]}], ";", "\[IndentingNewLine]", 
       "\[IndentingNewLine]", 
       RowBox[{"SSH2$ExecuteMultiLine", "[", 
        RowBox[{"{", 
         RowBox[{"\"\<cd [Cluster.WorkspaceDirectory]/Scripts\>\"", ",", 
          RowBox[{"Sequence", "@@", "chmodlist"}]}], "}"}], "]"}], ";", 
       "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"CheckDirectory", "[", "\"\<./temp\>\"", "]"}], ";", 
       "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"configtxt", "=", 
        RowBox[{"StringReplace", "[", 
         RowBox[{
          RowBox[{"Import", "[", 
           RowBox[{"\"\<./Scripts/config.m\>\"", ",", "\"\<Text\>\""}], "]"}],
           ",", 
          RowBox[{
           RowBox[{"\"\<\\\"[Local.PackageDirectory]\\\"\>\"", "~~", 
            RowBox[{"Shortest", "[", "value__", "]"}], "~~", "\"\<,\>\""}], 
           "\[RuleDelayed]", 
           RowBox[{
           "\"\<\\\"[Local.PackageDirectory]\\\" -> \\\"\>\"", "<>", 
            "packagedir2", "<>", "\"\<\\\",\>\""}]}]}], "]"}]}], ";", 
       "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"Export", "[", 
        RowBox[{
        "\"\<./temp/config.m\>\"", ",", "configtxt", ",", "\"\<Text\>\""}], 
        "]"}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
       RowBox[{"SSH2$UploadFile", "[", 
        RowBox[{
        "\"\<./temp/config.m\>\"", ",", "\"\<./Scripts/config.m\>\""}], "]"}],
        ";"}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", "]"}]}], ";"}],
   "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Cluster$InstallBootstrapper", "[", 
    RowBox[{
     RowBox[{"freshinstall_:", "False"}], ",", 
     RowBox[{"packagedir_:", "Automatic"}], ",", 
     RowBox[{"workspacedir_:", "Automatic"}]}], "]"}], ":=", 
   RowBox[{"Module", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{"workspacedir", "=!=", "Automatic"}], ",", 
        RowBox[{"Cluster$SetConfig", "[", 
         RowBox[{
         "\"\<[Cluster.WorkspaceDirectory]\>\"", ",", "workspacedir"}], 
         "]"}]}], "]"}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"Bootstrapper$Installer$Workspace", "[", 
       RowBox[{"freshinstall", ",", "packagedir"}], "]"}], ";", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"Bootstrapper$Installer$Packages", "[", 
       RowBox[{"freshinstall", ",", "packagedir"}], "]"}], ";", 
      "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"Return", "[", "True", "]"}], ";"}]}], "\[IndentingNewLine]", 
    "]"}]}], ";"}]}], "Code",
 CellChangeTimes->{{3.777208640067375*^9, 3.777208663282886*^9}, {
   3.7772101475689926`*^9, 3.7772101863372936`*^9}, {3.777210269903898*^9, 
   3.777210298004052*^9}, {3.7772103330765567`*^9, 3.7772104546368856`*^9}, {
   3.777210559576933*^9, 3.7772105812306595`*^9}, {3.7772106417147236`*^9, 
   3.7772106480059366`*^9}, {3.7772116579599857`*^9, 3.777211658192357*^9}, {
   3.777211955341901*^9, 3.7772119561387663`*^9}, {3.777211997206169*^9, 
   3.777212168848681*^9}, {3.7772136160142355`*^9, 3.777213721544587*^9}, {
   3.7772137650315228`*^9, 3.7772137709739027`*^9}, {3.7776428821744413`*^9, 
   3.7776429130120068`*^9}, {3.7776431997706614`*^9, 
   3.7776432412442064`*^9}, {3.7776434338103523`*^9, 
   3.7776436891413918`*^9}, {3.777643724589219*^9, 3.7776437694269133`*^9}, {
   3.7776438379907665`*^9, 3.777643841631657*^9}, {3.7776439143546696`*^9, 
   3.777644155781411*^9}, {3.777650497928206*^9, 3.7776505273844433`*^9}, {
   3.7776505767606516`*^9, 3.777650660462018*^9}, {3.7776506964884787`*^9, 
   3.777650725112794*^9}, {3.777650778945216*^9, 3.7776508247802467`*^9}, {
   3.7776522924262395`*^9, 3.777652396716323*^9}, 3.777652432176408*^9, {
   3.7776524925840635`*^9, 3.777652492856368*^9}, {3.777652533510337*^9, 
   3.777652573239422*^9}, {3.7776526650191035`*^9, 3.777652787126772*^9}, {
   3.777652837998042*^9, 3.7776528634602127`*^9}, {3.777652982112074*^9, 
   3.7776530002299476`*^9}, {3.7776533085076256`*^9, 3.777653317931468*^9}, {
   3.7776630277305403`*^9, 3.777663082998098*^9}, {3.777663136545847*^9, 
   3.7776631483602943`*^9}, {3.777663183764016*^9, 3.777663281785207*^9}, 
   3.7776633132822804`*^9, {3.7776634207020435`*^9, 3.777663425185416*^9}, {
   3.777663468459687*^9, 3.7776635193957825`*^9}, {3.7777105000660343`*^9, 
   3.777710508119235*^9}, {3.7777109015644183`*^9, 3.7777109330366974`*^9}, {
   3.7777109772833676`*^9, 3.7777110157936096`*^9}, {3.777711077413363*^9, 
   3.777711214016111*^9}, {3.777711248062705*^9, 3.777711311392578*^9}, {
   3.777711373435412*^9, 3.7777115798210964`*^9}, {3.7777118471967*^9, 
   3.7777118563980274`*^9}, 3.7777134414474077`*^9, {3.7777141969802876`*^9, 
   3.7777142024965944`*^9}, {3.777714560332249*^9, 3.77771456220955*^9}, {
   3.777716180193037*^9, 3.777716186486804*^9}, {3.7777172895402412`*^9, 
   3.777717300929839*^9}, {3.7777200902579594`*^9, 3.7777200981446233`*^9}, {
   3.7777201818122807`*^9, 3.777720217117216*^9}, {3.7777202499004126`*^9, 
   3.777720293991494*^9}, {3.7781719791530848`*^9, 3.7781719939200935`*^9}, {
   3.7781725755133085`*^9, 3.7781726643223724`*^9}, {3.778172710815374*^9, 
   3.77817271933595*^9}, {3.778173095894614*^9, 3.7781731551606307`*^9}, {
   3.778173228166592*^9, 3.7781732414552236`*^9}, {3.778173409979422*^9, 
   3.7781734272684655`*^9}, {3.77817357484892*^9, 3.7781735758352833`*^9}, {
   3.77817379565588*^9, 3.7781737985517178`*^9}, {3.7781742264409275`*^9, 
   3.778174253581929*^9}, {3.778174298095667*^9, 3.778174331135191*^9}, {
   3.778174532860946*^9, 3.77817453663795*^9}, {3.77817457486522*^9, 
   3.7781745788250933`*^9}, {3.7781748414396086`*^9, 3.7781748762607093`*^9}, 
   3.7781751701260953`*^9, {3.7781752440913076`*^9, 3.778175256443105*^9}, {
   3.7781752890743465`*^9, 3.7781752986488285`*^9}, {3.778175383567851*^9, 
   3.778175455399994*^9}, {3.778175528320401*^9, 3.7781756140510373`*^9}, {
   3.778215310752942*^9, 3.7782153160082903`*^9}, {3.7783043748234005`*^9, 
   3.778304402720851*^9}, {3.890741919524469*^9, 
   3.8907419414594526`*^9}},ExpressionUUID->"b890c373-52d0-4e3b-80af-\
f3bd313e2924"]
}, Open  ]]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1280, 555},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
ShowSelection->True,
Magnification:>1.2 Inherited,
FrontEndVersion->"11.1 for Microsoft Windows (64-bit) (May 16, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 155, 3, 85, "Section", "ExpressionUUID" -> \
"4497a1bc-9ac3-4cd6-9082-1050174a670c"],
Cell[737, 27, 12459, 253, 1448, "Code", "ExpressionUUID" -> \
"b890c373-52d0-4e3b-80af-f3bd313e2924"]
}, Open  ]]
}
]
*)


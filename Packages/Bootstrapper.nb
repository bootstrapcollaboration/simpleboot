(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     15242,        365]
NotebookOptionsPosition[     13510,        316]
NotebookOutlinePosition[     13935,        334]
CellTagsIndexPosition[     13892,        331]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Bootstrapper Files", "Section",
 CellChangeTimes->{{3.7766786173013973`*^9, 3.776678620838959*^9}, {
  3.77721253728283*^9, 
  3.777212547053301*^9}},ExpressionUUID->"4497a1bc-9ac3-4cd6-9082-\
1050174a670c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Bootstrapper$Version", "=", "\"\<4.0\>\""}], ";"}]], "Code",
 CellChangeTimes->{{3.7772125116937556`*^9, 3.7772125244179296`*^9}, 
   3.777213054480201*^9, {3.7781782266205544`*^9, 3.778178227081628*^9}, {
   3.7799070140152893`*^9, 3.779907014154951*^9}, {3.819248019594331*^9, 
   3.819248019792775*^9}, 3.8237643371389437`*^9, {3.826006961086233*^9, 
   3.8260069612664175`*^9}, {3.8282033616707993`*^9, 3.8282033623258147`*^9}, 
   3.8354514286768866`*^9, {3.8381748905987606`*^9, 3.838174890943709*^9}, {
   3.89076200639826*^9, 3.890762006645451*^9}, {3.890923064475199*^9, 
   3.8909230686561804`*^9}},ExpressionUUID->"6d3ca341-dfd8-45c5-a9a2-\
94b6d42d5008"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"Bootstrapper$Packages$MMAFiles", "=", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{
     "\"\<Infarestructure.m\>\"", ",", "\[IndentingNewLine]", 
      "\"\<GenericScalarBlock.m\>\"", ",", "\[IndentingNewLine]", 
      "\"\<BootstrapHelper.m\>\"", ",", "\[IndentingNewLine]", 
      "\"\<AutoCB.m\>\"", ",", "\n", "\"\<AutoCB3.m\>\"", ",", 
      "\[IndentingNewLine]", "\"\<SDPBAnalyzer.m\>\"", ",", 
      "\[IndentingNewLine]", "\"\<SDPB.m\>\"", ",", "\[IndentingNewLine]", 
      "\"\<SystemShell.m\>\"", ",", "\[IndentingNewLine]", 
      "\"\<RemoteSDPB.m\>\"", ",", "\[IndentingNewLine]", 
      "\"\<DelaunaySampler.m\>\"", ",", "\[IndentingNewLine]", 
      "\"\<BisectionSampler.m\>\"", ",", "\[IndentingNewLine]", 
      "\"\<ClusterManager.m\>\"", ",", "\n", "\"\<ClusterModels.m\>\"", ",", 
      "\[IndentingNewLine]", "\"\<ThetaSampler.m\>\"", ",", 
      "\[IndentingNewLine]", "\"\<ThetaSamplerDeltaMethod.m\>\"", ",", 
      "\[IndentingNewLine]", "\"\<Installer.m\>\"", ",", 
      "\[IndentingNewLine]", "\"\<lockfswap.m\>\"", ",", 
      "\[IndentingNewLine]", "\"\<Bootstrapper.m\>\"", ",", 
      "\[IndentingNewLine]", "\"\<boot.m\>\"", ",", "\n", "\n", 
      "\"\<SpecialUtilities.m\>\"", ",", "\n", "\"\<MPSolve.m\>\"", ",", 
      "\n", "\"\<SDPBCheckpoint.m\>\"", ",", "\n", "\n", 
      "\"\<ZhukovskyCoordinate.m\>\"", ",", "\n", 
      "\"\<LegacyCompatibility.m\>\""}], "\[IndentingNewLine]", "}"}]}], 
   ";"}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Bootstrapper$Packages$Executables", "=", 
    RowBox[{
    "{", "\[IndentingNewLine]", "\"\<lockfswap\>\"", "\[IndentingNewLine]", 
     "}"}]}], ";"}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Bootstrapper$Scripts$Executables", "=", 
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
    "\"\<RunMMA_job.sh\>\"", ",", "\[IndentingNewLine]", 
     "\"\<RunMMA_submit.sh\>\"", ",", "\[IndentingNewLine]", 
     "\"\<RunMMA_local.sh\>\"", ",", "\[IndentingNewLine]", "\"\<pvm2sdp\>\"",
      ",", "\[IndentingNewLine]", "\"\<sdpb_El\>\"", ",", 
     "\[IndentingNewLine]", "\"\<config.sh\>\""}], "\[IndentingNewLine]", 
    "}"}]}], ";"}]}], "Code",
 CellChangeTimes->{{3.7772086932256517`*^9, 3.77720879488546*^9}, {
   3.777208841461133*^9, 3.7772088515860944`*^9}, {3.7772089701916313`*^9, 
   3.777209029056902*^9}, {3.7772090737177134`*^9, 3.777209181890572*^9}, {
   3.7772094673712273`*^9, 3.777209467599638*^9}, {3.777209581505927*^9, 
   3.7772095849981265`*^9}, 3.777210110060393*^9, 3.7772103784702587`*^9, {
   3.7772124085081453`*^9, 3.7772124088702016`*^9}, {3.7772127401757956`*^9, 
   3.7772127404420843`*^9}, {3.7772139581405816`*^9, 3.7772139646098776`*^9}, 
   3.777215540128093*^9, {3.7772166789613028`*^9, 3.7772166837086062`*^9}, {
   3.77722628478727*^9, 3.777226291998314*^9}, {3.7774599004523954`*^9, 
   3.7774599134573565`*^9}, {3.777642134072917*^9, 3.777642311535381*^9}, 
   3.7776425389726057`*^9, {3.7776428319226885`*^9, 3.7776428326158743`*^9}, {
   3.77765248437435*^9, 3.777652485213144*^9}, {3.7787369964482684`*^9, 
   3.778737002060257*^9}, {3.7787373175760565`*^9, 3.7787373181077547`*^9}, {
   3.8237642675081654`*^9, 3.823764308383853*^9}, {3.8237695622633443`*^9, 
   3.8237695634498425`*^9}, {3.8281620003812084`*^9, 
   3.8281620066205196`*^9}, {3.830548621319408*^9, 3.8305486287219453`*^9}, {
   3.835451436661781*^9, 3.835451447365099*^9}, {3.836366223429677*^9, 
   3.836366224449444*^9}, {3.8381748848247395`*^9, 3.838174886616787*^9}, {
   3.870016694625624*^9, 3.8700167027586412`*^9}, {3.890761986929573*^9, 
   3.8907619993879366`*^9}, {3.8929145282375717`*^9, 
   3.892914548243372*^9}},ExpressionUUID->"e248acd2-19b9-4a7e-9587-\
1b5306a44324"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Load MMA packages", "Section",
 CellChangeTimes->{{3.7766786173013973`*^9, 3.776678620838959*^9}, {
  3.7772125553326387`*^9, 
  3.7772125581860256`*^9}},ExpressionUUID->"4ac0377a-3ceb-473d-b5d8-\
d4780fefbc0c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Bootstrapper$Package$MMAFiles$AvoidLoad", "=", 
   RowBox[{"{", 
    RowBox[{"\"\<Bootstrapper.m\>\"", ",", "\"\<boot.m\>\""}], "}"}]}], 
  ";"}]], "Code",ExpressionUUID->"630c32b7-f8d0-4c11-abb9-d1c2b2af2297"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{"(*", 
       RowBox[{
        RowBox[{"Print", "[", 
         RowBox[{"\"\<Load \>\"", "<>", "#"}], "]"}], ";"}], "*)"}], 
      "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"Get", "[", 
        RowBox[{
         RowBox[{"ReconfigCmd", "@", "\"\<[Local.PackageDirectory]/\>\""}], "<>",
          "#"}], "]"}], ";"}], ")"}], "&"}], "/@", 
    RowBox[{"DeleteCases", "[", 
     RowBox[{"Bootstrapper$Packages$MMAFiles", ",", 
      RowBox[{
      "Alternatives", "@@", "Bootstrapper$Package$MMAFiles$AvoidLoad"}]}], 
     "]"}]}], ";"}], "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{"Print", "[", 
   RowBox[{
   "\"\<Bootstapper packages Loaded. Version : \>\"", ",", 
    "Bootstrapper$Version"}], "]"}], ";"}]}], "Code",
 CellChangeTimes->{{3.7772124664185452`*^9, 3.7772124812818594`*^9}, {
   3.777212562534029*^9, 3.7772126068874474`*^9}, {3.7772139722826924`*^9, 
   3.7772139785763893`*^9}, 3.7772140785291*^9, {3.777214139519414*^9, 
   3.777214139834566*^9}, {3.777215560563092*^9, 3.7772156252225285`*^9}, {
   3.777216224263587*^9, 3.7772162344893494`*^9}, {3.777642359702694*^9, 
   3.7776423805501556`*^9}, {3.777642522847988*^9, 3.777642547495338*^9}, {
   3.778172296726923*^9, 3.7781722971816664`*^9}, {3.815698279981124*^9, 
   3.8156982879154363`*^9}, {3.8156983353397875`*^9, 3.815698373031572*^9}, 
   3.8156987703896437`*^9, {3.920581044611066*^9, 
   3.9205810742252293`*^9}},ExpressionUUID->"c93b67c0-b69c-4bd8-ab02-\
3f3744a4e1b0"],

Cell[CellGroupData[{

Cell["initialize all global variables here", "Subsection",
 CellChangeTimes->{{3.815697875800105*^9, 
  3.815697877950976*^9}},ExpressionUUID->"d0595072-1dcc-4259-8c0f-\
9b7f1928d1bc"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Bootstrapper$SetMMAPrecision", "[", "precicion_", "]"}], ":=", 
   RowBox[{"Module", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Bootstrapper$prec", "=", "precicion"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"$MinPrecision", "=", "Bootstrapper$prec"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"rstar", "=", 
       RowBox[{
        RowBox[{"3", "-", 
         RowBox[{"2", 
          SqrtBox["2"]}]}], "//", "SetPrec"}]}], ";", "\[IndentingNewLine]", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"this", " ", "is", " ", "used", " ", "in", " ", 
        RowBox[{"SDPB", ".", "m"}]}], " ", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"rhoCrossing", " ", "=", " ", 
       RowBox[{"SetPrecision", "[", 
        RowBox[{
         RowBox[{"3", "-", 
          RowBox[{"2", " ", 
           RowBox[{"Sqrt", "[", "2", "]"}]}]}], ",", " ", "precicion"}], 
        "]"}]}], ";", "\[IndentingNewLine]", "\[IndentingNewLine]", 
      RowBox[{"Print", "[", 
       RowBox[{
       "\"\<MMA Precision set to \>\"", ",", "precicion", ",", "\"\<.\>\""}], 
       "]"}], ";"}]}], "\[IndentingNewLine]", "]"}]}], ";"}]], "Code",
 CellChangeTimes->{{3.8156985555264287`*^9, 3.815698595283353*^9}, {
  3.8156987146296473`*^9, 
  3.815698751407259*^9}},ExpressionUUID->"2fa8b09a-37ce-4b85-b09b-\
eaaeaacdeb5e"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Bootstrapper$SetMMAPrecision", "[", "200", "]"}], ";"}]], "Code",
 CellChangeTimes->{{3.815697983474881*^9, 
  3.8156980095422325`*^9}},ExpressionUUID->"713ed043-5c0b-495a-a0d1-\
75fe24be63fe"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["config.m related routine", "Section",
 CellChangeTimes->{{3.7766786173013973`*^9, 3.776678620838959*^9}, {
  3.7772125553326387`*^9, 3.7772125581860256`*^9}, {3.7889176437536707`*^9, 
  3.7889176501465*^9}},ExpressionUUID->"4621f94e-1515-431b-9475-892933d2dd5f"],

Cell["\<\
config.m only defines minimal routine that allow simpleboot to boot. More \
complicated function will be defined here.\
\>", "Text",
 CellChangeTimes->{{3.78891769545825*^9, 3.788917706012287*^9}, {
  3.7889177663798094`*^9, 3.7889178037536707`*^9}, {3.7889187850938826`*^9, 
  3.7889187863564706`*^9}},ExpressionUUID->"ffcb5822-61db-4882-9d3d-\
dc314726b677"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Cluster$GetConfig", "[", 
     RowBox[{"itemname_", ",", 
      RowBox[{"default_:", "None"}]}], "]"}], ":=", 
    RowBox[{"Module", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"{", "rslt", "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"rslt", "=", 
        RowBox[{"itemname", "/.", "Cluster$Configuration"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{"rslt", "===", "itemname"}], " ", ",", "\[IndentingNewLine]", 
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{"default", "===", "None"}], ",", "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Print", "[", 
             RowBox[{"\"\<Cluster$GetConfig failure:\>\"", ",", "rslt"}], 
             "]"}], ";", 
            RowBox[{"Abort", "[", "]"}]}], ",", "\[IndentingNewLine]", 
           "default"}], "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", 
         ",", "rslt"}], "]"}]}]}], "\[IndentingNewLine]", "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Cluster$SetConfig", "[", 
    RowBox[{"itemname_", ",", "value_", ",", 
     RowBox[{"addnewvalueQ_:", "True"}]}], "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"If", "[", 
       RowBox[{
        RowBox[{"FreeQ", "[", 
         RowBox[{"Cluster$Configuration", ",", 
          RowBox[{"(", 
           RowBox[{"itemname", "->", "_"}], ")"}], ",", 
          RowBox[{"{", "1", "}"}]}], "]"}], ",", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{"addnewvalueQ", "===", "False"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{
            RowBox[{"Print", "[", 
             RowBox[{
             "\"\<Cluster$SetConfig fail:\>\"", ",", "itemname", ",", 
              "\"\< was not in config set. Abort.\>\""}], "]"}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"Abort", "[", "]"}], ";"}]}], "\[IndentingNewLine]", 
          "]"}], ";", "\[IndentingNewLine]", 
         RowBox[{"AppendTo", "[", 
          RowBox[{"Cluster$Configuration", ",", 
           RowBox[{"itemname", "->", "None"}]}], "]"}], ";"}]}], 
       "\[IndentingNewLine]", "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"Cluster$Configuration", "=", 
       RowBox[{"Cluster$Configuration", "/.", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"itemname", "->", "_"}], ")"}], "->", 
         RowBox[{"(", 
          RowBox[{"itemname", "->", "value"}], ")"}]}]}]}]}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}]}], "Code",
 CellChangeTimes->{{3.7889176777007265`*^9, 3.7889176893922386`*^9}, {
  3.788917923014781*^9, 3.7889179646699095`*^9}, {3.7889180137392263`*^9, 
  3.78891801390977*^9}, {3.7889180487249365`*^9, 3.7889181179980073`*^9}, {
  3.7889185075226736`*^9, 3.7889185705687876`*^9}, {3.788918656077813*^9, 
  3.7889186616621475`*^9}, {3.788918738592371*^9, 
  3.788918742003994*^9}},ExpressionUUID->"01f96c17-915d-44c3-b9cc-\
6884ae06474e"]
}, Open  ]],

Cell["", "Section",
 CellChangeTimes->{{3.7766786173013973`*^9, 3.776678620838959*^9}, {
   3.7772125553326387`*^9, 3.7772125581860256`*^9}, {3.7889176437536707`*^9, 
   3.7889176501465*^9}, {3.81569776875515*^9, 3.8156977794295626`*^9}, 
   3.8156978734889917`*^9},ExpressionUUID->"9cfd101d-768b-418e-a3b2-\
a1050dc4cfc1"]
},
AutoGeneratedPackage->Automatic,
WindowSize->{1264, 596},
WindowMargins->{{-8, Automatic}, {Automatic, 0}},
ShowSelection->True,
Magnification:>1.2 Inherited,
FrontEndVersion->"11.1 for Microsoft Windows (64-bit) (May 16, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 213, 4, 85, "Section", "ExpressionUUID" -> \
"4497a1bc-9ac3-4cd6-9082-1050174a670c"],
Cell[795, 28, 701, 11, 61, "Code", "ExpressionUUID" -> \
"6d3ca341-dfd8-45c5-a9a2-94b6d42d5008"],
Cell[1499, 41, 3816, 65, 942, "Code", "ExpressionUUID" -> \
"e248acd2-19b9-4a7e-9587-1b5306a44324"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5352, 111, 217, 4, 85, "Section", "ExpressionUUID" -> \
"4ac0377a-3ceb-473d-b5d8-d4780fefbc0c"],
Cell[5572, 117, 245, 5, 61, "Code", "ExpressionUUID" -> \
"630c32b7-f8d0-4c11-abb9-d1c2b2af2297"],
Cell[5820, 124, 1565, 36, 128, "Code", "ExpressionUUID" -> \
"c93b67c0-b69c-4bd8-ab02-3f3744a4e1b0"],
Cell[CellGroupData[{
Cell[7410, 164, 184, 3, 58, "Subsection", "ExpressionUUID" -> \
"d0595072-1dcc-4259-8c0f-9b7f1928d1bc"],
Cell[7597, 169, 1468, 35, 310, "Code", "ExpressionUUID" -> \
"2fa8b09a-37ce-4b85-b09b-eaaeaacdeb5e"],
Cell[9068, 206, 228, 5, 61, "Code", "ExpressionUUID" -> \
"713ed043-5c0b-495a-a0d1-75fe24be63fe"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9345, 217, 268, 3, 85, "Section", "ExpressionUUID" -> \
"4621f94e-1515-431b-9475-892933d2dd5f"],
Cell[9616, 222, 370, 7, 35, "Text", "ExpressionUUID" -> \
"ffcb5822-61db-4882-9d3d-dc314726b677"],
Cell[9989, 231, 3179, 75, 502, "Code", "ExpressionUUID" -> \
"01f96c17-915d-44c3-b9cc-6884ae06474e"]
}, Open  ]],
Cell[13183, 309, 323, 5, 748, "Section", "ExpressionUUID" -> \
"9cfd101d-768b-418e-a3b2-a1050dc4cfc1"]
}
]
*)


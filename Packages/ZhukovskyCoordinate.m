(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



(* \!\(
\*SubsuperscriptBox[\(\[PartialD]\), \(y\), \(n\)]\(\(=\)\(
\*SubsuperscriptBox[\(\[Sum]\), \(m = 0\), \(n\)]
\*SubscriptBox[\(B\), \(nm\)]\ 
\*SubsuperscriptBox[\(\[PartialD]\), \(z\), \(m\)]\)\)\) *)
Zhukovsky$B[n_,m_]:=Zhukovsky$B[n,m]=ComplexExpand@BellY[n,m,Table[1/2 (\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
RowBox[{
RowBox[{"-", "2"}], " ", 
RowBox[{"Cos", "[", 
RowBox[{
FractionBox["1", "2"], " ", 
RowBox[{"(", 
RowBox[{
RowBox[{"-", "1"}], "-", "K2"}], ")"}], " ", "\[Pi]"}], "]"}], " ", 
RowBox[{"K2", "!"}]}], 
RowBox[{"K2", ">=", "1"}]},
{"1", 
TagBox["True",
"PiecewiseDefault",
AutoDelete->True]}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\)),{K2,1,1+n-m}]];
Zhukovsky$BInv[i_,j_]:=Zhukovsky$BInv$Tab[[1+i,1+j]];

(* 
Zhukovsky$BInvTab : Inverse of Subscript[B, n,m] ;

Zhukovsky$BIJ$Tab$Even : Use PartialDerivativeSet$Even[\[CapitalLambda]max] as a basis. Let I={i,j} where i\[GreaterEqual]j, i+j\[LessEqual]\[CapitalLambda]max and even. Zhukovsky$BIJ$Tab$Even is the matrix defined by
Subsuperscript[\[ScriptCapitalB], I,J, (even)]=Subscript[B, I[[1]],J[[1]]]Subscript[B, I[[2]],J[[2]]] for J[[1]]=J[[2]] and Subsuperscript[\[ScriptCapitalB], I,J, (even)]=Subscript[B, I[[1]],J[[1]]]Subscript[B, I[[2]],J[[2]]]+Subscript[B, I[[1]],J[[2]]]Subscript[B, I[[2]],J[[1]]] for J[[1]]\[NotEqual]J[[2]];
With this definition, \!\(
\*SubsuperscriptBox[\(\[PartialD]\), \(y\), \(m\)]\(
\*SubsuperscriptBox[\(\[PartialD]\), 
OverscriptBox[\(y\), \(_\)], \(n\)]F[z, 
\*OverscriptBox[\(z\), \(_\)]]\)\)=\!\(
\*SubscriptBox[\(\[Sum]\), \(i \[LessEqual] m\)]\(
\*SubscriptBox[\(\[Sum]\), \(j \[LessEqual] n\)]
\*SubscriptBox[\(B\), \(m, i\)]
\*SubscriptBox[\(B\), \(n, j\)]
\*SubsuperscriptBox[\(\[PartialD]\), \(z\), \(i\)]\(
\*SubsuperscriptBox[\(\[PartialD]\), 
OverscriptBox[\(z\), \(_\)], \(j\)]F[z, 
\*OverscriptBox[\(z\), \(_\)]]\)\ can\ be\ rewrite\ as\ I\)\)=(m,n) , J=(i,j) with i\[GreaterEqual]j , Subscript[(\!\(
\*SubsuperscriptBox[\(\[PartialD]\), \(y\), \(m\)]\(
\*SubsuperscriptBox[\(\[PartialD]\), 
OverscriptBox[\(y\), \(_\)], \(n\)]F[z, 
\*OverscriptBox[\(z\), \(_\)]]\)\)), I]=Subscript[B, I,J]Subscript[(\!\(
\*SubsuperscriptBox[\(\[PartialD]\), \(z\), \(i\)]\(
\*SubsuperscriptBox[\(\[PartialD]\), 
OverscriptBox[\(z\), \(_\)], \(j\)]F[z, 
\*OverscriptBox[\(z\), \(_\)]]\)\)), J] ;

Zhukovsky$BInvTab$\[CapitalLambda]max$Even : the inverse matrix of Subsuperscript[\[ScriptCapitalB], I,J, (even)];

Zhukovsky$BInvTab$\[CapitalLambda]max$Odd : same as above except even \[Rule] odd
 *)
Clear[Zhukovsky$\[CapitalLambda]max,
Zhukovsky$\[CapitalLambda]max$EvenBasis,Zhukovsky$\[CapitalLambda]max$OddBasis,
Zhukovsky$\[CapitalLambda]max$Even$Dim,Zhukovsky$\[CapitalLambda]max$Odd$Dim,
Zhukovsky$BIJ$Tab$Even,Zhukovsky$BIJ$Tab$Odd,
Zhukovsky$BIJ$Inv$Tab$Odd,Zhukovsky$BIJ$Inv$Tab$Even,
Zhukovsky$B$Tab,Zhukovsky$BInv$Tab];
Zhukovsky$InitializeBInv[\[CapitalLambda]max_]:=Module[
{pdsete,pdseto},
Zhukovsky$\[CapitalLambda]max=\[CapitalLambda]max;
Zhukovsky$\[CapitalLambda]max$EvenBasis=pdsete=PartialDerivativeSet$Even[\[CapitalLambda]max];
Zhukovsky$\[CapitalLambda]max$OddBasis=pdseto=PartialDerivativeSet$Odd[\[CapitalLambda]max];

Zhukovsky$\[CapitalLambda]max$Even$Dim=Length@pdsete;
Zhukovsky$\[CapitalLambda]max$Odd$Dim=Length@pdseto;
Zhukovsky$BIJ$Tab$Odd=Table[
If[j[[1]]==j[[2]],Zhukovsky$B[i[[1]],j[[1]]]Zhukovsky$B[i[[2]],j[[2]]],Zhukovsky$B[i[[1]],j[[1]]]Zhukovsky$B[i[[2]],j[[2]]]+Zhukovsky$B[i[[1]],j[[2]]]Zhukovsky$B[i[[2]],j[[1]]]]
,{i,pdseto},{j,pdseto}];
Zhukovsky$BIJ$Tab$Even=Table[
If[j[[1]]==j[[2]],Zhukovsky$B[i[[1]],j[[1]]]Zhukovsky$B[i[[2]],j[[2]]],Zhukovsky$B[i[[1]],j[[1]]]Zhukovsky$B[i[[2]],j[[2]]]+Zhukovsky$B[i[[1]],j[[2]]]Zhukovsky$B[i[[2]],j[[1]]]]
,{i,pdsete},{j,pdsete}];
Zhukovsky$BIJ$Inv$Tab$Odd=Zhukovsky$BIJ$Tab$Odd//Inverse;
Zhukovsky$BIJ$Inv$Tab$Even=Zhukovsky$BIJ$Tab$Even//Inverse;

Zhukovsky$B$Tab=Table[Zhukovsky$B[n,m],{n,0,\[CapitalLambda]max},{m,0,\[CapitalLambda]max}];
Zhukovsky$BInv$Tab=Zhukovsky$B$Tab//Inverse;
];

(* Vector$COC$zzb2Zhu, Vector$COC$Zhu2zzb, DualVector$COC$zzb2Zhu, DualVector$COC$Zhu2zzb:
those function will only access F$zzb[m,n] (or F$zhu...) with m+n even/odd and m\[GreaterEqual]n if i+j is even/odd *)

(* compute Subscript[F$zhu, i,j] = Subscript[B, i,m] Subscript[B, j,n] Subscript[F$zzb, m,n] *)
Vector$COC$zzb2Zhu[F$zzb_,{i_,j_}]:=Sum[If[#!=0,If[m>=n,F$zzb[m,n],F$zzb[n,m]]#,0]&[Zhukovsky$B[i,m]Zhukovsky$B[j,n]],{m,0,Zhukovsky$\[CapitalLambda]max},{n,0,Zhukovsky$\[CapitalLambda]max}];
(* compute Subscript[F$zzb, i,j] = \!\(
\*SubsuperscriptBox[\(B\), \(i, m\), \(-1\)]\(\ \)
\*SubsuperscriptBox[\(B\), \(j, n\), \(-1\)]\(\ \)\)Subscript[F$zhu, m,n] *)
Vector$COC$Zhu2zzb[F$zhu_,{i_,j_}]:=Sum[If[#!=0,If[m>=n,F$zhu[m,n],F$zhu[n,m]]#,0]&[Zhukovsky$BInv[i,m]Zhukovsky$BInv[j,n]],{m,0,Zhukovsky$\[CapitalLambda]max},{n,0,Zhukovsky$\[CapitalLambda]max}];

(* compute Subscript[\[Alpha]$zhu, i,j] = Subscript[\[Alpha]$zzb, m,n] Subsuperscript[B, m,i, -1] Subsuperscript[B, n,j, -1]  *)
DualVector$COC$zzb2Zhu[\[Alpha]$zzb_,{i_,j_}]:=
Sum[If[#!=0,If[m>=n,\[Alpha]$zzb[m,n],\[Alpha]$zzb[n,m]]#,0]&[Zhukovsky$BInv[m,i]Zhukovsky$BInv[n,j]],
{m,0,Zhukovsky$\[CapitalLambda]max},{n,0,Zhukovsky$\[CapitalLambda]max}];

(* compute Subscript[\[Alpha]$zzb, i,j] = Subscript[\[Alpha]$zhu, m,n] Subscript[B, m,i] Subscript[B, n,j] *)
DualVector$COC$Zhu2zzb[\[Alpha]$zhu_,{i_,j_}]:=
Sum[If[#!=0,If[m>=n,\[Alpha]$zhu[m,n],\[Alpha]$zhu[n,m]]#,0]&[Zhukovsky$B[m,i]Zhukovsky$B[n,j]],
{m,0,Zhukovsky$\[CapitalLambda]max},{n,0,Zhukovsky$\[CapitalLambda]max}];

(* compute Subscript[w, I]=\!\(
\*SubsuperscriptBox[\(B\), \(I, J\), \((even)\)]
\*SubscriptBox[\(v\), \(J\)]\) *)
Vector$COC$zzb2Zhu$Even[vec_]:=Zhukovsky$BIJ$Tab$Even.vec;
(* compute Subscript[w, I]=\!\(
\*SubsuperscriptBox[\(B\), \(I, J\), \((odd)\)]
\*SubscriptBox[\(v\), \(J\)]\) *)
Vector$COC$zzb2Zhu$Odd[vec_]:=Zhukovsky$BIJ$Tab$Odd.vec;
(* compute Subscript[w, J]=Subscript[v, I]Subsuperscript[B, I,J, (even),-1] *)
DualVector$COC$zzb2Zhu$Even[vec_]:=vec.Zhukovsky$BIJ$Inv$Tab$Even;
(* compute Subscript[w, J]=Subscript[v, I]Subsuperscript[B, I,J, (odd),-1] *)
DualVector$COC$zzb2Zhu$Odd[vec_]:=vec.Zhukovsky$BIJ$Inv$Tab$Odd;

(* compute Subscript[w, I]=\!\(
\*SubsuperscriptBox[\(B\), \(I, J\), \(\((even)\), \(-1\)\)]
\*SubscriptBox[\(v\), \(J\)]\) *)
Vector$COC$Zhu2zzb$Even[vec_]:=Zhukovsky$BIJ$Inv$Tab$Even.vec;
(* compute Subscript[w, I]=\!\(
\*SubsuperscriptBox[\(B\), \(I, J\), \(\((odd)\), \(-1\)\)]
\*SubscriptBox[\(v\), \(J\)]\) *)
Vector$COC$Zhu2zzb$Odd[vec_]:=Zhukovsky$BIJ$Inv$Tab$Odd.vec;
(* compute Subscript[w, J]=Subscript[v, I]Subsuperscript[B, I,J, (even)] *)
DualVector$COC$Zhu2zzb$Even[vec_]:=vec.Zhukovsky$BIJ$Tab$Even;
(* compute Subscript[w, J]=Subscript[v, I]Subsuperscript[B, I,J, (odd)] *)
DualVector$COC$Zhu2zzb$Odd[vec_]:=vec.Zhukovsky$BIJ$Tab$Odd;


TakeRowComponent[alpha_,conf$pd_,row_]:=alpha[[TranslatePDIndex[conf$pd,row]]];

(* conf$pd must generated from PartialDerivativeSet$Even, PartialDerivativeSet$Odd *)
FullVector$ApplyCOCfunc[fullvec_,conf$pd_,{COCfunc$even_,COCfunc$odd_}]:=Module[
{\[CapitalLambda]max,row$evenQ,len$row,vec$row,
pdindex,takealpha},

len$row=Length@conf$pd;
\[CapitalLambda]max=Max@Flatten[conf$pd];
row$evenQ=EvenQ/@Plus@@@conf$pd[[All,1,1]];

Zhukovsky$InitializeBInv[\[CapitalLambda]max];

Module[{row=#},
vec$row=TakeRowComponent[fullvec,conf$pd,row];
If[row$evenQ[[row]],COCfunc$even,COCfunc$odd][vec$row]
]&/@Range[1,len$row]//Flatten
];

Alpha$COC$Zhu2zzb[alpha$zhu_,conf$pd_]:=FullVector$ApplyCOCfunc[alpha$zhu,conf$pd,{DualVector$COC$Zhu2zzb$Even,DualVector$COC$Zhu2zzb$Odd}];
Alpha$COC$zzb2Zhu[alpha$zzb_,conf$pd_]:=FullVector$ApplyCOCfunc[alpha$zzb,conf$pd,{DualVector$COC$zzb2Zhu$Even,DualVector$COC$zzb2Zhu$Odd}];

F$COC$Zhu2zzb[F$zhu_,conf$pd_]:=FullVector$ApplyCOCfunc[F$zhu,conf$pd,{Vector$COC$Zhu2zzb$Even,Vector$COC$Zhu2zzb$Odd}];
F$COC$zzb2Zhu[F$zzb_,conf$pd_]:=FullVector$ApplyCOCfunc[F$zzb,conf$pd,{Vector$COC$zzb2Zhu$Even,Vector$COC$zzb2Zhu$Odd}];




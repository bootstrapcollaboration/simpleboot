(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     12155,        299]
NotebookOptionsPosition[     10494,        263]
NotebookOutlinePosition[     10898,        279]
CellTagsIndexPosition[     10855,        276]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "This is a long exercise set. But the computation is pretty quick for ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[CapitalLambda]", "=", "11"}], TraditionalForm]],ExpressionUUID->
  "1e9db4f2-9ddb-4998-9a3a-323b91275305"],
 " (around 10mins to ~1 hour).\n\nFor perimeter user:\nWe are sharing limited \
computational resources among many participants. Please \
don\[CloseCurlyQuote]t do heavy computations. For debugging, you can use \
debugq. Once the code runs correctly for 10mins, you can stop it and re-run \
it on defq. Once the feasible region have rough sharp (usually around 100 to \
200 points), you can stop the computation.\n\nTry to do Exercise 1-3 today. \
Try to finish Exercise 4 by Thursday (it\[CloseCurlyQuote]s related to what I \
want to discuss on Thursday)."
}], "Text",
 CellChangeTimes->{{3.890943094624915*^9, 3.890943153143484*^9}, {
  3.890943526869833*^9, 3.890943716463705*^9}, {3.8909437508438396`*^9, 
  3.8909439229734545`*^9}, {3.8913023133891716`*^9, 
  3.891302349047311*^9}},ExpressionUUID->"e1f3d74c-283f-4993-8615-\
839f85404401"],

Cell[CellGroupData[{

Cell["Exercise 1", "Subsection",
 CellChangeTimes->{{3.890935353433055*^9, 
  3.8909353555366244`*^9}},ExpressionUUID->"0729bbab-9213-47ad-9609-\
21ef75ed6d81"],

Cell["\<\
Go through IsingOE.nb to understand how the bootstrap conditions are set up.
Execute IsingOE.nb to scan a 3D Ising island.\
\>", "Text",
 CellChangeTimes->{{3.89093791927205*^9, 3.890937956188779*^9}, {
  3.8909379910853868`*^9, 3.890938006428544*^9}, {3.8909396400745983`*^9, 
  3.890939649391693*^9}, {3.890941003010291*^9, 3.8909410180210257`*^9}, {
  3.890989452723214*^9, 
  3.8909894864029446`*^9}},ExpressionUUID->"54ebec50-1017-4c78-a3d5-\
6619587334f6"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Exercise 2", "Subsection",
 CellChangeTimes->{{3.890935353433055*^9, 3.8909353555366244`*^9}, {
  3.8909379620843225`*^9, 
  3.8909379622078915`*^9}},ExpressionUUID->"30782628-9321-458c-bf77-\
194ef641caa4"],

Cell["\<\
Go through IsingO.nb to understand how to use user-defined variables.
Execute IsingO.nb to scan single correlator for 3D Ising.\
\>", "Text",
 CellChangeTimes->{{3.89093791927205*^9, 3.890937977537956*^9}, 
   3.890938013170263*^9, {3.8909387732075787`*^9, 3.8909387912869215`*^9}, 
   3.890939652710341*^9, {3.8909425964204035`*^9, 3.890942611081728*^9}, {
   3.8909895103532114`*^9, 
   3.890989531505972*^9}},ExpressionUUID->"e64016e7-d93f-4a07-8e54-\
92dc7758f777"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Exercise 3", "Subsection",
 CellChangeTimes->{{3.890935353433055*^9, 3.8909353555366244`*^9}, {
  3.8909379620843225`*^9, 3.8909379622078915`*^9}, {3.8909387992569656`*^9, 
  3.8909387993759747`*^9}, {3.890939625189433*^9, 3.890939625284872*^9}, {
  3.8909404699705706`*^9, 
  3.8909404700845203`*^9}},ExpressionUUID->"d3e7e262-fb1e-499b-8800-\
7714f1c87ef1"],

Cell[TextData[{
 "Modify IsingO.nb to scan single correlator bound for 3D O(N). You can use \
the crossing vector from ArXiv:1307.6856 (2.5).\n\nIn simpleboot, the default \
definition of conformal block is in ArXiv:1805.04405 TABLE I row 1, while the \
convention of ArXiv:1307.6856 is in row 5. Therefore there is a ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"-", "1"}], ")"}], "\[ScriptL]"], TraditionalForm]],
  ExpressionUUID->"ccf1c51d-b16c-4ede-8018-fa59ef4b15c0"],
 " difference for ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["V", 
    RowBox[{"A", ",", "\[CapitalDelta]", ",", "\[ScriptL]"}]], 
   TraditionalForm]],ExpressionUUID->"2f109523-e832-4bf2-a6e9-02f7c94d38af"],
 ". When you type in crossing vector, you should take minus of ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["V", 
    RowBox[{"A", ",", "\[CapitalDelta]", ",", "\[ScriptL]"}]], 
   TraditionalForm]],ExpressionUUID->"ec9b3f5b-bc0b-4956-9dfd-61e81ba6975f"],
 " in ArXiv:1307.6856 (2.5)."
}], "Text",
 CellChangeTimes->{{3.89093791927205*^9, 3.890937977537956*^9}, 
   3.890938013170263*^9, {3.8909387732075787`*^9, 3.890938815200221*^9}, {
   3.8909388937243166`*^9, 3.8909390269367085`*^9}, {3.8909390711203136`*^9, 
   3.8909390854291515`*^9}, {3.890939161813181*^9, 3.890939213297784*^9}, {
   3.8909392458298197`*^9, 3.890939265677021*^9}, {3.8909393337736034`*^9, 
   3.8909394274962606`*^9}, {3.8909895383863363`*^9, 
   3.890989550116977*^9}},ExpressionUUID->"2d2d86e9-5a85-4efb-a899-\
e1c832cdb9fa"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Exercise 5", "Subsection",
 CellChangeTimes->{{3.890935353433055*^9, 3.8909353555366244`*^9}, {
  3.890939627820876*^9, 3.890939627992419*^9}, {3.89130206935717*^9, 
  3.8913020739061575`*^9}},ExpressionUUID->"9945ad10-ec8d-4fe0-9a86-\
9fe8265203ef"],

Cell[TextData[{
 "Modify IsingOE.nb to scan ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["\[CapitalDelta]", "\[Sigma]"], TraditionalForm]],
  ExpressionUUID->"06d3cb48-5e1b-4bb1-83c7-5ca5171c3553"],
 " v.s. ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["\[CapitalDelta]", 
    RowBox[{"\[Sigma]", "'"}]], TraditionalForm]],ExpressionUUID->
  "3bd1289b-1033-416a-b045-486a5bf67f60"],
 " . Fix ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["\[CapitalDelta]", "\[Epsilon]"], TraditionalForm]],
  ExpressionUUID->"a67434f8-56f0-484a-9045-a6d8538b8347"],
 " to be roughly in the center of the ",
 Cell[BoxData[
  FormBox[
   RowBox[{"(", 
    RowBox[{
     SubscriptBox["\[CapitalDelta]", "\[Sigma]"], ",", 
     SubscriptBox["\[CapitalDelta]", "\[Epsilon]"]}], ")"}], 
   TraditionalForm]],ExpressionUUID->"46b1a746-496c-4021-b34b-64aeb601c5f0"],
 " island."
}], "Text",
 CellChangeTimes->{{3.890935843384507*^9, 3.8909358518048644`*^9}, {
   3.89093591495302*^9, 3.890935946831679*^9}, {3.8909360643328905`*^9, 
   3.890936295920776*^9}, {3.8909368774935417`*^9, 3.8909369780587564`*^9}, 
   3.89093952696504*^9, {3.8909423795501394`*^9, 3.8909427121168485`*^9}, {
   3.891302134136956*^9, 3.8913021547838316`*^9}, {3.89130220020442*^9, 
   3.891302236297842*^9}, {3.891302281724867*^9, 
   3.8913023048105907`*^9}},ExpressionUUID->"475b91a4-339c-40ab-9744-\
10e7e253d28f"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Exercise 6", "Subsection",
 CellChangeTimes->{{3.890935353433055*^9, 3.8909353555366244`*^9}, {
  3.890939627820876*^9, 3.890939627992419*^9}, {3.89130206935717*^9, 
  3.891302069476918*^9}},ExpressionUUID->"2c9bb8e6-78c1-4177-aab8-\
b7d776b4b23d"],

Cell[TextData[{
 "arXiv:1612.08471 Table 2 has scaling dimensions of many operators. If we \
demand some operators, such as ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[Epsilon]", "'"}], TraditionalForm]],ExpressionUUID->
  "112f7d9d-15e4-48b5-9a4b-b4a1de2e81d3"],
 ", are located within a small windows around their true value, how does the \
island changes? Let\[CloseCurlyQuote]s explore it in this exercise.\n\nFirst, \
modify IsingOE.nb function GapConfiguration[dim_,lset_] to set up some \
interval positivity tests. I show you an explicitely example here for ",
 Cell[BoxData[
  FormBox[
   RowBox[{"\[Epsilon]", "'"}], TraditionalForm]],ExpressionUUID->
  "1f027098-5e89-4aa5-93d1-d29320996c4b"],
 " :"
}], "Text",
 CellChangeTimes->{{3.890935843384507*^9, 3.8909358518048644`*^9}, {
   3.89093591495302*^9, 3.890935946831679*^9}, {3.8909360643328905`*^9, 
   3.890936295920776*^9}, {3.8909368774935417`*^9, 3.8909369780587564`*^9}, 
   3.89093952696504*^9, {3.8909423795501394`*^9, 
   3.8909427121168485`*^9}},ExpressionUUID->"3cf79fa0-f8cd-4d50-9122-\
bfc68ed9afef"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"{", 
   RowBox[{
    RowBox[{"op", "[", 
     RowBox[{"op", ",", " ", "\"\<E\>\"", ",", " ", "1", ",", " ", "1"}], 
     "]"}], ",", " ", 
    RowBox[{"IntervalPositivity", "[", 
     RowBox[{"3.82965", ",", "3.82991"}], "]"}], ",", "0"}], "}"}], "    ", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"\[Alpha]", ".", 
      SubscriptBox["V", "even"]}], ">", 
     RowBox[{"0", " ", "for", " ", "3.82965"}], ">", "\[CapitalDelta]", ">", 
     RowBox[{"3.82991", " ", "and", " ", "L"}]}], "=", "0"}], " ", 
   "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"{", 
   RowBox[{
    RowBox[{"op", "[", 
     RowBox[{"op", ",", " ", "\"\<E\>\"", ",", " ", "1", ",", " ", "1"}], 
     "]"}], ",", " ", "6.8913", ",", "0"}], "}"}], "    ", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"\[Alpha]", ".", 
      SubscriptBox["V", "even"]}], ">", 
     RowBox[{"0", " ", "for", " ", "3.82965"}], ">", "\[CapitalDelta]", ">", 
     RowBox[{"3.82991", " ", "and", " ", "L"}]}], "=", "0"}], " ", 
   "*)"}]}]}], "Code",
 CellChangeTimes->{{3.8373540902311163`*^9, 3.837354103422883*^9}, {
   3.8373579953531103`*^9, 3.8373579956632094`*^9}, {3.8374073390802307`*^9, 
   3.8374073488604593`*^9}, {3.8375189386931114`*^9, 
   3.8375189395034275`*^9}, {3.8375273044122057`*^9, 3.837527308645511*^9}, 
   3.8382063267096653`*^9, {3.890467732926046*^9, 3.890467739733567*^9}, {
   3.8909083022989297`*^9, 3.8909083262133284`*^9}, {3.8909083572415085`*^9, 
   3.890908365853548*^9}, {3.8909177974927893`*^9, 3.8909178275877285`*^9}, {
   3.8909235771601667`*^9, 3.890923589142191*^9}, {3.8909236322186327`*^9, 
   3.890923635849083*^9}, {3.8909239566461253`*^9, 3.8909240657594805`*^9}, {
   3.890940554604514*^9, 3.8909405588073378`*^9}, {3.890942742844882*^9, 
   3.8909428844928713`*^9}},ExpressionUUID->"527c7962-464b-4892-b740-\
012def2e528f"],

Cell[TextData[{
 "where I took the error window of ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{"\[Epsilon]", "'"}], ",", 
    RowBox[{"\[Epsilon]", "''"}]}], TraditionalForm]],ExpressionUUID->
  "a246e38b-8807-4870-8946-b3f7b977da63"],
 " from arXiv:1612.08471. \n\nUse IndividualOperator[3] to isolate the EMT \
and add a gap after that.  Scan this island. You may add more windows around \
other operators to explore how the island changes."
}], "Text",
 CellChangeTimes->{{3.890942955635746*^9, 
  3.890943089660684*^9}},ExpressionUUID->"d7b4ac31-ea6e-48a3-80ba-\
4988ed3887c4"]
}, Open  ]]
},
WindowSize->{1070.25, 555.},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"13.0 for Microsoft Windows (64-bit) (February 4, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"4f320d37-cdf3-4ddc-b78b-264b0115fc8b"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1084, 19, 196, "Text",ExpressionUUID->"e1f3d74c-283f-4993-8615-839f85404401"],
Cell[CellGroupData[{
Cell[1667, 43, 160, 3, 54, "Subsection",ExpressionUUID->"0729bbab-9213-47ad-9609-21ef75ed6d81"],
Cell[1830, 48, 472, 9, 58, "Text",ExpressionUUID->"54ebec50-1017-4c78-a3d5-6619587334f6"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2339, 62, 213, 4, 54, "Subsection",ExpressionUUID->"30782628-9321-458c-bf77-194ef641caa4"],
Cell[2555, 68, 479, 9, 58, "Text",ExpressionUUID->"e64016e7-d93f-4a07-8e54-92dc7758f777"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3071, 82, 365, 6, 54, "Subsection",ExpressionUUID->"d3e7e262-fb1e-499b-8800-7714f1c87ef1"],
Cell[3439, 90, 1525, 32, 104, "Text",ExpressionUUID->"2d2d86e9-5a85-4efb-a899-e1c832cdb9fa"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5001, 127, 256, 4, 54, "Subsection",ExpressionUUID->"9945ad10-ec8d-4fe0-9a86-9fe8265203ef"],
Cell[5260, 133, 1368, 34, 35, "Text",ExpressionUUID->"475b91a4-339c-40ab-9744-10e7e253d28f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6665, 172, 254, 4, 54, "Subsection",ExpressionUUID->"2c9bb8e6-78c1-4177-aab8-b7d776b4b23d"],
Cell[6922, 178, 1076, 22, 104, "Text",ExpressionUUID->"3cf79fa0-f8cd-4d50-9122-bfc68ed9afef"],
Cell[8001, 202, 1884, 42, 68, "Code",ExpressionUUID->"527c7962-464b-4892-b740-012def2e528f"],
Cell[9888, 246, 590, 14, 104, "Text",ExpressionUUID->"d7b4ac31-ea6e-48a3-80ba-4988ed3887c4"]
}, Open  ]]
}
]
*)


#!/bin/bash
#SBATCH --ntasks-per-node=56
#SBATCH --nodes=1
#SBATCH --time=7-00:00:00
#SBATCH --mem-per-cpu=6G
#SBATCH --partition=expansion


echo This jobs is $PBS_JOBID@$PBS_QUEUE, on host:
hostname
echo CPU Information:
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
pwd

echo "Project director:"
echo $SLURM_PROJECTDIR

echo "running MMA file:"
echo $SLURM_MMAFILE

echo "with parameter:"
echo $SLURM_MMAPARAM

echo Number of tasks : $SLURM_NTASKS
echo Job-id : $SLURM_JOB_ID

export cores=${SLURM_NTASKS}
export cores_per_node=$((${SLURM_NTASKS} / ${SLURM_NNODES}))
echo "Environment for sdpb scripts"
echo cores: ${cores}
echo cores_per_node: ${cores_per_node}

source ./Scripts/config.sh

MathKernel -script $SLURM_MMAFILE $SLURM_MMAPARAM

(* ::Package:: *)

reehorstBase="/central/groups/dssimmon/reehorst/packages_mar7/packages";
Cluster$Configuration = {
    "[Cluster.LoginServer]" -> "caltech",
    "[Cluster.Account]" -> "ningsu",
    "[Cluster.WorkspaceDirectory]" -> "/central/groups/dssimmon/ning/tutorial_testrun",
    "[Cluster.PackageDirectory]" -> "/central/groups/dssimmon/ning/simpleboot4_package/",
    (* "[Local.PackageDirectory]" -> "/home/apiazza/simpleboot/Packages", *)
    "[Local.PackageDirectory]" -> "C:\\Users\\shinn\\OneDrive\\Bootstrap_Projects\\simpleboot_Latest\\simpleboot4",
    
    "[SCP]" -> "cmdwslrun scp",
    "[SSH]" -> "cmdwslrun ssh",
     
    "[AutoCB3.scalar_blocks_mod.script]" -> "mpirun --bind-to none -n 1 "<>FileNameJoin[{reehorstBase, "scalarblocks_mod/install/bin/scalar_blocks_mod"}]<>" --num-threads $cores_per_node",
    "[AutoCB3.sdp2input_mod.script]" -> "mpirun -n $cores --bind-to none "<>FileNameJoin[{reehorstBase, "sdp2input_mod_2.5.0/install/bin/sdp2input_mod_2.5.0"}],
    "[AutoCB3.sdpdd]" -> "mpirun -n $cores "<>FileNameJoin[{reehorstBase, "sdpdd_bBcA_hessian/sdpb/build/sdpdd_hessian_ij"}]<>" --procsPerNode=$cores_per_node ",
    "[sdpb.script]" -> "mpirun -n $cores "<>FileNameJoin[{reehorstBase, "sdpb2.5.1/sdpb/build/sdpb"}]<>" --procsPerNode $cores_per_node",
    "[DynamicSDPB.script]" -> "./Scripts/dynamical_sdpb_El.sh", (* probably need a fix *)

    "[AutoCB3.spectrumpy]"-> "source /home/apiazza/repos/spectrum-extraction/venv/bin/activate && /home/apiazza/repos/spectrum-extraction/spectrum.py",
    "[AutoCB3.unisolve]"->FileNameJoin[{reehorstBase, "MPSolve-2.2/unisolve"}],

    "[ClusterModel]" -> "srun",
    "[SDPB.Version]"->"sdpb_El",
    "[QUADRATICNET.SCRIPT]"->"stack exec -- quadratic-net-exe --pvm2sdpExecutable /home/ningsu/stack/quadratic-net-master/scripts/pvm2sdp.sh"<>" --sdpbExecutable /home/ningsu/stack/quadratic-net-master/scripts/sdpb.sh", (* probably need a fix *)
    "[QUADRATICNET.SWITCH]"->False,

    "[UsePackage.scalar_block]"->False,
    "[UsePackage.sdp2input]"->False,
    "[Debug.TimingStatistics]"->True,

    "[UsePackage.AutoCB3]"->True,
    "[AutoCB3.no-fake-poles]"->True,
    "[AutoCB3.sdp2input_mod]"->True,
    "[AutoCB3.scalar_blocks_mod.cmdprec]"->120,
    "[AutoCB3.scalar_blocks_mod.precision]"->1024,
    "[AutoCB3.sdp2input_mod.precision]"->1024
};


(* ::Text:: *)
(*Note sure I took the correct sdpdd_hessian_ij or whether I accidentally build it twice. There is also one at:*)
(*/central/groups/dssimmon/reehorst/packages_mar7/packages/sdpd/install/bin*)
(*Both seem to run correctly.*)


ReconfigCmd[str_]:=FixedPoint[StringReplace[#,Cluster$Configuration]&,str];
SSH$ReconfigCmd=ReconfigCmd;

#!/bin/bash

source /etc/profile

module load gcc-runtime/11.3.1-gcc-11.3.1-jw6fhud cmake/3.20.2-gcc-11.3.1-ivdnfrp openblas/0.3.23-gcc-11.3.1-cu3huj2 openmpi/4.1.2-gcc-11.3.1-fs7u3kb eigen/3.4.0-gcc-11.3.1-jvlgwff libxml2/2.10.3-gcc-11.3.1-m4mk5ab metis/5.1.0-gcc-11.3.1-tpf4hr6 gmp/6.2.1-gcc-11.3.1-rwggvnn
module unload boost
module load mpfr/4.2.0-gcc-11.3.1-fd6eusl
module load mathematica/12.3

# spectrum.py needs python. Moreover you will have to install the required modules.
# E.g. with
# python -m ensurepip --user
# python -m pip install --user mpmath lxml scipy numpy

# ADD PATH TO MPSOLVE for spectrum.py
export PATH="/central/groups/dssimmon/reehorst/packages_mar7/packages/MPSolve-2.2:$PATH"

module load python/3.11.6-gcc-11.3.1-rphh4kv

# You should probably just load libiconv/1.17-gcc-11.3.1-h3jdn2x instead of adding its path explicitly below but the below is what I did and it worked.
export LD_LIBRARY_PATH=/central/software9/spack/opt/spack/linux-rhel9-x86_64/gcc-11.3.1/libiconv-1.17-h3jdn2xdnmrfkduqff5omnrkkyz4danb/lib:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH=/central/software9/spack/opt/spack/linux-rhel9-x86_64/gcc-11.3.1/mpfr-4.2.0-fd6euslm2s5zuio3vepc4ue4crfhwcuf/lib:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH=/central/software9/spack/opt/spack/linux-rhel9-x86_64/gcc-11.3.1/gmp-6.2.1-rwggvnnfya2va2n3zzstnqnuohjd745z/lib:$LD_LIBRARY_PATH

export PATH=/home/ningsu/stack/stack-2.1.3-linux-x86_64:/central/home/ningsu/stack/quadratic-net-master/.stack-work/install/x86_64-linux/4cb579b782f06779b162a62e4431b893fa3b821ec2794c06e97befcf277cae38/8.2.2/bin:$PATH
export OPENBLAS_NUM_THREADS=1




if [[ -z "${SLURM_TASKS_PER_NODE}" ]]; then
  export phys_cores_per_node=$(lscpu | awk '/^Core.s. per socket:/ {cores=$NF} /^Socket.s.:/ {sockets=$NF} END {print cores * sockets}')
  export phys_cores_per_job=$(lscpu | awk '/^Core.s. per socket:/ {cores=$NF} /^Socket.s.:/ {sockets=$NF} END {print cores * sockets}')
  export cores=$phys_cores_per_node
  export cores_per_node=$phys_cores_per_node
else
  export phys_cores_per_node=$(echo $SLURM_TASKS_PER_NODE| cut --complement -d'(' -f 2)
  export phys_cores_per_job=$(( $phys_cores_per_node * $SLURM_JOB_NUM_NODES ))
  export cores=${SLURM_NTASKS}
  export cores_per_node=$((${SLURM_NTASKS} / ${SLURM_NNODES}))
fi


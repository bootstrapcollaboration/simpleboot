#!/bin/bash
#SBATCH --nodes=1
#SBATCH --partition=expansion
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=2
#SBATCH --time=7-00:00:00


echo This jobs is $PBS_JOBID@$PBS_QUEUE, on host:
hostname
echo CPU Information:
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
pwd

echo "Project director:"
echo $SLURM_PROJECTDIR

echo "running MMA file:"
echo $SLURM_MMAFILE

echo "with parameter:"
echo $SLURM_MMAPARAM

echo Number of tasks : $SLURM_NTASKS
echo Job-id : $SLURM_JOB_ID

source ./Scripts/config.sh

MathKernel -script $SLURM_MMAFILE $SLURM_MMAPARAM

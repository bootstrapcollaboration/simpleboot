#!/bin/bash

source /gpfs/ddn/teorici/Vichi_Group/Packages/packages/gcc_9.4.0_installation/setup.sh
source /gpfs/ddn/teorici/Vichi_Group/Packages/packages/open_mpi_4_1_2_with_local_gcc/installation2/setup.sh

export PATH=$PATH:/afs/pi.infn.it/pisw/Wolfram/11/Executables

export phys_cores_per_node=$(lscpu | awk '/^Core.s. per socket:/ {cores=$NF} /^Socket.s.:/ {sockets=$NF} END {print cores * sockets}')



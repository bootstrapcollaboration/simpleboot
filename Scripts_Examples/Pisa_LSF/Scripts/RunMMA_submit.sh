#!/bin/bash

export SLURM_MMAFILE=$1
export SLURM_MMAPARAM=$2
export SLURM_PROJECTDIR=$3

mkdir -p $SLURM_PROJECTDIR

# try to pretend to be SLURM
#jobIDstr=$(bsub -o ./$SLURM_PROJECTDIR/btjob-%J.out -e ./$SLURM_PROJECTDIR/btjob-%J.out < ./Scripts/RunMMA_job.sh)
#jobID=$(echo $jobIDstr | cut -d "<" -f2 | cut -d ">" -f1)
#echo Submitted batch job $jobID

bsub -o ./$SLURM_PROJECTDIR/btjob-%J.out -e ./$SLURM_PROJECTDIR/btjob-%J.out < ./Scripts/RunMMA_job.sh


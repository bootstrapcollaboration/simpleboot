(* ::Package:: *)

 Cluster$Configuration = {
    "[Cluster.LoginServer]" -> "172.20.20.250",
    "[Cluster.Account]" -> "nsu2",
    (* Workspace directory on remote cluster *)
    "[Cluster.WorkspaceDirectory]" -> "/gpfs/[Cluster.Account]/O3vstt4_SB4_debug",
    (* remote Packages directory *)
    "[Cluster.PackageDirectory]" -> "/gpfs/nsu2/simpleboot4_package",
    
    (* local Packages directory *)
    "[Local.PackageDirectory]" -> "/gpfs/nsu2/simpleboot4_package",
    
    "[AutoCB3.scalar_blocks_mod.script]"->
    "mpirun --bind-to none -n 1 /home/nsu2/packages/scalarblocks_sdp2input_mod/install/scalar_blocks_mod --num-threads $phys_cores_per_node", (* The script to call scalar_blocks_mod *)
    "[AutoCB3.sdp2input_mod.script]"->
    "mpirun -n $phys_cores_per_node --bind-to none /home/nsu2/packages/scalarblocks_sdp2input_mod/install/sdp2input_mod_2.5.1 ", (* The script to call sdp2input_mod *)
    "[AutoCB3.sdpdd]"->
    False, (* The script to call sdpdd *)
    "[sdpb.script]"->
    "mpirun -n $phys_cores_per_job /home/nsu2/packages/sdpb2.5.1/bin/sdpb --procsPerNode $phys_cores_per_node", (* The script to call SDPB *)
    "[DynamicSDPB.script]"->
    "mpirun -n $phys_cores_per_job /home/nsu2/packages/dynamical_navigator_merged_RV1C/bin/dynamical_sdp_RV1C --procsPerNode $phys_cores_per_node", (* The script to call skydiving *)
    
    
    (* no need to modify the items below *)
    "[ClusterModel]" -> "srun",
    "[SDPB.Version]"->"sdpb_El",
    "[QUADRATICNET.SCRIPT]"->"/home/nsu2/packages/stack/stack-2.5.1-linux-x86_64/stack exec -- /home/nsu2/packages/quadratic-net-old/quadratic-net-exe --pvm2sdpExecutable /home/nsu2/packages/quadratic-net-old/quadratic-net-master/scripts/pvm2sdp.sh --sdpbExecutable /home/nsu2/packages/quadratic-net-old/quadratic-net-master/scripts/sdpb.sh --workDir tmp",
    "[QUADRATICNET.SWITCH]"->False,
    
    "[UsePackage.scalar_block]"->False,
    "[UsePackage.sdp2input]"->False,
    "[Debug.TimingStatistics]"->True,
    
    (* AutoCB3 configuration *)
    "[UsePackage.AutoCB3]"->True,
    "[AutoCB3.no-fake-poles]"->True,
    "[AutoCB3.sdp2input_mod]"->True,
    "[AutoCB3.scalar_blocks_mod.cmdprec]"->120,
    "[AutoCB3.scalar_blocks_mod.precision]"->1024,
    "[AutoCB3.sdp2input_mod.precision]"->1024
}


ReconfigCmd[str_]:=FixedPoint[StringReplace[#,Cluster$Configuration]&,str];
SSH$ReconfigCmd=ReconfigCmd;

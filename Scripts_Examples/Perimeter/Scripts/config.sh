#!/bin/bash

module load slurm
module load mathematica/13.3
module load gcc/10.2.0
module load python/3.7

export PATH=/gpfs/nsu2/packages/sdp2input_fix:$PATH:/home/nsu2/packages/install/MPSolve-2.2:/home/nsu2/packages/spectrum-extraction/spectrum-extraction-master:/home/nsu2/packages/install/MPSolve/usr/local/bin/

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/nsu2/packages/lapack/lib/:/home/nsu2/packages/install/MPSolve/usr/local/lib/:/home/nsu2/packages/boost/boost_1_68_0_install/lib/:


if [[ -z "${SLURM_TASKS_PER_NODE}" ]]; then
  export phys_cores_per_node=$(lscpu | awk '/^Core.s. per socket:/ {cores=$NF} /^Socket.s.:/ {sockets=$NF} END {print cores * sockets}')
  export phys_cores_per_job=$(lscpu | awk '/^Core.s. per socket:/ {cores=$NF} /^Socket.s.:/ {sockets=$NF} END {print cores * sockets}')
else
  export phys_cores_per_node=$(echo $SLURM_TASKS_PER_NODE| cut --complement -d'(' -f 2)
  export phys_cores_per_job=$(( $phys_cores_per_node * $SLURM_JOB_NUM_NODES ))
fi



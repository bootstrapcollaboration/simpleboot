#!/bin/bash
#SBATCH --nodes=1
#SBATCH --partition=amddebugq
#SBATCH --time=01:00:00
#SBATCH --cpus-per-task=2


date
echo CPU Information:
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
pwd

echo "Project director:"
echo $SLURM_PROJECTDIR

echo "running MMA file:"
echo $SLURM_MMAFILE

echo "with parameter:"
echo $SLURM_MMAPARAM

source ./Scripts/config.sh

MathKernel -script $SLURM_MMAFILE $SLURM_MMAPARAM






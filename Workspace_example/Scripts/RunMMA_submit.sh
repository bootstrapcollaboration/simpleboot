#!/bin/bash

export SLURM_MMAFILE=$1
export SLURM_MMAPARAM=$2
export SLURM_PROJECTDIR=$3

mkdir -p $SLURM_PROJECTDIR

sbatch  --account $(whoami) --output=./$SLURM_PROJECTDIR/btjob-%j.out ./Scripts/RunMMA_job.sh

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --partition=defq
#SBATCH --time=0-10:00:00

date
echo CPU Information:
cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
pwd

echo "Project director:"
echo $SLURM_PROJECTDIR

echo "running MMA file:"
echo $SLURM_MMAFILE

echo "with parameter:"
echo $SLURM_MMAPARAM


MathKernel -script $SLURM_MMAFILE $SLURM_MMAPARAM





